class HttpError extends Error {
  constructor (error) {
    super()
    this.message = 'HTTP Error'
    this.status = error.status
    this.resCode = error.resCode
    this.resDesc = error.resDesc
  }
}

class ServiceError extends Error {
  constructor (error, payload) {
    super()
    this.message = 'Service Error'
    this.status = error.status
    this.resCode = error.resCode
    this.resDesc = error.resDesc
    this.payload = payload
  }
}

module.exports = {
  HttpError,
  ServiceError
}
