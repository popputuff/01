const Sequelize = require('sequelize')
const dayjs = require('dayjs')

dayjs.extend(require('dayjs/plugin/utc'))

const settlementError = require('../constants/errors/settlementError')

const transactionSettlementBatchStatusMaster = require('../constants/masters/transactionSettlementBatchStatusMaster')
const transactionStatusMaster = require('../constants/masters/transactionStatusMaster')

const sequelize = require('../helpers/sequelize')
const { ServiceError } = require('../helpers/error')

const masterTransactionTypeModel = require('../models/masterTransactionTypeModel')
const partnerModel = require('../models/partnerModel')
const transactionModel = require('../models/transactionModel')
const transactionSettlementBatchModel = require('../models/transactionSettlementBatchModel')

const Op = Sequelize.Op

exports.findTransactionSettlementBatches = () => (req, res, next) => {
  const partnerID = req.headers['x-partner-id']
  const {
    start_date: startDate,
    end_date: endDate,
    batch_no: batchNo,
    status
  } = req.query

  const offset = req.query.offset ? Number(req.query.offset) : 0
  const limit = req.query.limit ? Number(req.query.limit) : 10

  const query = {
    include: [
      {
        model: partnerModel,
        as: 'partner',
        required: true
      },
      {
        model: masterTransactionTypeModel,
        as: 'transactionType',
        required: true
      }
    ],
    where: {},
    order: [['createdAt', 'DESC']],
    offset,
    limit
  }

  if (partnerID) query.where.partnerId = partnerID
  if (startDate && endDate) query.where.batchDate = { [Op.between]: [startDate, endDate] }
  if (batchNo) query.where.batchNo = batchNo
  if (status) query.where.status = status

  transactionSettlementBatchModel.findAndCountAll(query)
    .then(({ count, rows }) => {
      req.total = count
      req.batches = rows
    })
    .then(() => next())
    .catch((err) => next(err))
}

exports.findTransactionSettlementBatch = () => (req, res, next) => {
  const id = req.params.id

  const query = {
    where: { id }
  }

  transactionSettlementBatchModel.findOne(query)
    .then((batch) => {
      if (!batch) {
        throw new ServiceError(settlementError.ERR_SETTLEMENT_BATCH_NOT_FOUND)
      } else {
        req.batch = batch
      }
    })
    .then(() => next())
    .catch((err) => next(err))
}

exports.processTransactionSettlementBatch = () => (req, res, next) => {
  const batch = req.batch

  if (batch.status !== transactionSettlementBatchStatusMaster.PENDING) {
    next()
  } else {
    batch.update({ status: transactionSettlementBatchStatusMaster.PROCESSING })
      .then(() => next())
      .catch((err) => next(err))
  }
}

exports.settleTransactionSettlementBatch = () => async (req, res, next) => {
  const batch = req.batch

  const {
    operator_id: operatorID,
    oeprator_name: operatorName
  } = req.body

  const settledAt = dayjs.utc().format()

  if (batch.status !== transactionSettlementBatchStatusMaster.PROCESSING) {
    next(new ServiceError(settlementError.ERR_SETTLEMENT_BATCH_STATUS_IS_NOT_PROCESSING))
  } else {
    const t = await sequelize.transaction()
    try {
      const query = {
        attributes: ['id'],
        where: {
          batchNo: batch.batchNo
        }
      }
      const transactions = await transactionModel.findAll(query)
      for await (const transaction of transactions) {
        await transaction.update({
          status: transactionStatusMaster.SETTLED,
          settledById: operatorID,
          settledByName: operatorName,
          settledAt: settledAt
        }, { transaction: t })
      }
      await batch.update({ status: transactionSettlementBatchStatusMaster.SETTLED }, { transaction: t })
      await t.commit()
      next()
    } catch (err) {
      await t.rollback()
      next(err)
    }
  }
}
