const transactionSummaryModel = require('../models/transactionSummaryModel')

exports.incrementTransactionSummary = () => (req, res, next) => {
  const t = req.t
  const transaction = req.transaction

  transactionSummaryModel.findOne({
    where: {
      partnerId: transaction.partnerId,
      transactionTypeId: transaction.transactionTypeId,
      transactionSubTypeId: transaction.transactionSubTypeId
    },
    transaction: t,
    lock: t.LOCK.UPDATE
  })
    .then((transactionSummary) => {
      if (!transactionSummary) {
        return transactionSummaryModel.create({
          partnerId: transaction.partnerId,
          transactionTypeId: transaction.transactionTypeId,
          transactionSubTypeId: transaction.transactionSubTypeId,
          daily: transaction.total,
          weekly: transaction.total,
          monthly: transaction.total,
          yearly: transaction.total
        }, { transaction: t })
      } else {
        return transactionSummary.increment([
          'daily',
          'weekly',
          'monthly',
          'yearly'
        ], { by: transaction.total, transaction: t })
      }
    })
    .then(() => next())
    .catch((err) => next(err))
}

exports.decrementTransactionSummary = () => (req, res, next) => {
  const t = req.t
  const transaction = req.transaction

  transactionSummaryModel.findOne({
    where: {
      partnerId: transaction.partnerId,
      transactionTypeId: transaction.transactionTypeId,
      transactionSubTypeId: transaction.transactionSubTypeId
    },
    transaction: t,
    lock: t.LOCK.UPDATE
  })
    .then((transactionSummary) => {
      return transactionSummary.decrement([
        'daily',
        'weekly',
        'monthly',
        'yearly'
      ], { by: transaction.total, transaction: t })
    })
    .then(() => next())
    .catch((err) => next(err))
}
