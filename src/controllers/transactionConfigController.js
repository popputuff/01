const transactionConfigModel = require('../models/transactionConfigModel')

exports.findTransactionConfig = () => (req, res, next) => {
  const t = req.t
  const transaction = req.transaction
  const query = {
    where: {
      partnerId: transaction.partnerId,
      transactionTypeId: transaction.transactionTypeId,
      transactionSubTypeId: transaction.transactionSubTypeId
    },
    transaction: t
  }
  transactionConfigModel.findOne(query)
    .then((transactionConfig) => {
      req.transactionConfig = transactionConfig
    })
    .then(() => next())
    .catch((err) => next(err))
}
