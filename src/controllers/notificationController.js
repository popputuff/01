const axios = require('axios')
const dayjs = require('dayjs')

dayjs.extend(require('dayjs/plugin/utc'))
dayjs.extend(require('dayjs/plugin/timezone'))

exports.sendNotification = () => (req, res, next) => {
  const transaction = req.transaction

  const timezone = req.headers['x-timezone']
  const dateFormat = req.headers['x-date-format']

  axios.request({
    method: 'POST',
    url: transaction.notifyUrl,
    data: {
      type: transaction.type.name,
      sub_type: transaction.subType.name,
      reference_no: transaction.referenceNo,
      order_id: transaction.orderId,
      payer_name: transaction.payerName,
      payee_name: transaction.payeeName,
      net: Number(transaction.net),
      fee: Number(transaction.fee),
      discount: Number(transaction.discount),
      total: Number(transaction.total),
      currency: transaction.currency,
      status: transaction.status,
      approval_code: transaction.approvalCode,
      failed_reason: transaction.failedReason,
      created_at: dayjs.utc(transaction.createdAt).tz(timezone).format(dateFormat),
      holded_at: transaction.holdedAt ? dayjs.utc(transaction.holdedAt).tz(timezone).format(dateFormat) : null,
      approved_at: transaction.approvedAt ? dayjs.utc(transaction.approvedAt).tz(timezone).format(dateFormat) : null,
      cancelled_at: transaction.cancelledAt ? dayjs.utc(transaction.cancelledAt).tz(timezone).format(dateFormat) : null,
      failed_at: transaction.failedAt ? dayjs.utc(transaction.failedAt).tz(timezone).format(dateFormat) : null,
      voided_at: transaction.voidedAt ? dayjs.utc(transaction.voidedAt).tz(timezone).format(dateFormat) : null,
      refunded_at: transaction.refundedAt ? dayjs.utc(transaction.refundedAt).tz(timezone).format(dateFormat) : null,
      settled_at: transaction.settledAt ? dayjs.utc(transaction.settledAt).tz(timezone).format(dateFormat) : null,
      updated_at: dayjs.utc(transaction.updatedAt).tz(timezone).format(dateFormat)
    }
  })
    .catch((err) => console.error(err))
  next()
}
