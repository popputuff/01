const dayjs = require('dayjs')
const { customAlphabet } = require('nanoid')
const dictionary = require('nanoid-dictionary')
const Sequelize = require('sequelize')
const { Joi } = require('express-validation')
const Queue = require('bull')

dayjs.extend(require('dayjs/plugin/utc'))
dayjs.extend(require('dayjs/plugin/timezone'))

const transactionError = require('../constants/errors/transactionError')

const transactionStatusMaster = require('../constants/masters/transactionStatusMaster')
const transactionSubTypeMaster = require('../constants/masters/transactionSubTypeMaster')
const transactionTypeMaster = require('../constants/masters/transactionTypeMaster')
const walletTypeMaster = require('../constants/masters/walletTypeMaster')

const { ServiceError } = require('../helpers/error')

const masterBankModel = require('../models/masterBankModel')
const masterTransactionSubTypeModel = require('../models/masterTransactionSubTypeModel')
const masterTransactionTypeModel = require('../models/masterTransactionTypeModel')
const partnerModel = require('../models/partnerModel')
const transactionLimitModel = require('../models/transactionLimitModel')
const transactionModel = require('../models/transactionModel')
const transactionSummaryModel = require('../models/transactionSummaryModel')
const userModel = require('../models/userModel')
const walletLimitModel = require('../models/walletLimitModel')
const walletModel = require('../models/walletModel')

const REDIS_HOST = process.env.REDIS_HOST
const REDIS_PORT = process.env.REDIS_PORT

const Op = Sequelize.Op

exports.createTransaction = (transactionType, transactionSubType, payerWalletType, payeeWalletType) => (req, res, next) => {
  const system = req.system
  const partner = req.partner

  const t = req.t
  const payer = req.payer
  const payee = req.payee

  const partnerId = req.headers['x-partner-id']
  const currency = req.headers['x-currency']

  const referenceNo = dayjs.utc().format('YYYYMMDDHHmmssSSS') + customAlphabet(dictionary.numbers, 3)()
  const orderId = req.body.order_id
  const payerWallet = payerWalletType ? req.payer.wallets.find((wallet) => wallet.walletTypeId === payerWalletType.id) : req.payer.wallets[0]
  const payeeWallet = payeeWalletType ? req.payee.wallets.find((wallet) => wallet.walletTypeId === payeeWalletType.id) : req.payee.wallets[0]
  const discountCode = req.body.discount_code
  const net = Number(req.body.amount)
  const fee = req.fee ? Number(req.fee) : 0
  const discount = req.body.discount ? Number(req.body.discount) : 0
  const total = net + fee - discount
  const callbackUrl = req.body.callback_url
  const notifyUrl = req.body.notify_url
  const reference1 = req.body.reference1
  const reference2 = req.body.reference2
  const reference3 = req.body.reference3
  const remark = req.body.remark
  const expiryAt = req.body.expiry ? dayjs.utc().add(req.body.expiry || 1, 'minutes').format() : null

  if (!payerWallet) {
    return next(new ServiceError(transactionError.ERR_TRANSACTION_PAYER_WALLET_NOT_FOUND))
  }
  if (!payeeWallet) {
    return next(new ServiceError(transactionError.ERR_TRANSACTION_PAYEE_WALLET_NOT_FOUND))
  }

  if (payerWallet.walletId === payeeWallet.walletId) {
    return next(new ServiceError(transactionError.ERR_TRANSACTION_PAYER_SAME_AS_PAYEE))
  }

  let bankId
  let bankBranch
  let bankAccountNumber
  let bankAccountName
  if (
    transactionType.id === transactionTypeMaster.WITHDRAW.id &&
    transactionSubType.id === transactionSubTypeMaster.CASA.id
  ) {
    bankId = payer.bankId
    bankBranch = payer.bankBranch
    bankAccountNumber = payer.bankAccountNumber
    bankAccountName = payer.bankAccountName
  }

  let feeExudeId = null
  let feeExudeName = null
  let feeExudeWalletId = null
  if (fee > 0) {
    if (
      transactionType.id === transactionTypeMaster.TOPUP.id ||
      transactionType.id === transactionTypeMaster.WITHDRAW.id
    ) {
      feeExudeId = system.id
      feeExudeName = system.lastnameEN ? `${system.firstnameEN} ${system.lastnameEN}` : system.firstnameEN
      feeExudeWalletId = system.wallets.find((wallet) => wallet.walletTypeId === walletTypeMaster.SYSTEM.id).walletId
    } else {
      feeExudeId = partner.id
      feeExudeName = partner.lastnameEN ? `${partner.firstnameEN} ${partner.lastnameEN}` : partner.firstnameEN
      feeExudeWalletId = partner.wallets.find((wallet) => wallet.walletTypeId === walletTypeMaster.PARTNER.id).walletId
    }
  }

  let discountAbsorbId = null
  let discountAbsorbName = null
  let discountAbsorbWalletId = null
  if (discount > 0) {
    discountAbsorbId = partner.id
    discountAbsorbName = partner.lastnameEN ? `${partner.firstnameEN} ${partner.lastnameEN}` : partner.firstnameEN
    discountAbsorbWalletId = partner.wallets.find((wallet) => wallet.walletTypeId === walletTypeMaster.DISCOUNT.id).walletId
  }

  if (total < 0) {
    return next(new ServiceError(transactionError.ERR_TRANSACTION_DISCOUNT_HIGHER_THAN_TOTAL))
  }

  const transaction = {
    partnerId,
    transactionTypeId: transactionType.id,
    transactionSubTypeId: transactionSubType.id,
    referenceNo,
    orderId,
    payerId: payer.id,
    payerUid: payer.uid,
    payerName: payer.lastnameEN ? `${payer.firstnameEN} ${payer.lastnameEN}` : payer.firstnameEN,
    payerWalletId: payerWallet.walletId,
    payeeId: payee.id,
    payeeUid: payee.uid,
    payeeName: payee.lastnameEN ? `${payee.firstnameEN} ${payee.lastnameEN}` : payee.firstnameEN,
    payeeWalletId: payeeWallet.walletId,
    bankId,
    bankBranch,
    bankAccountNumber,
    bankAccountName,
    feeExudeId,
    feeExudeName,
    feeExudeWalletId,
    discountAbsorbId,
    discountAbsorbName,
    discountAbsorbWalletId,
    discountCode,
    net,
    fee,
    discount,
    total,
    currency,
    status: transactionStatusMaster.PENDING,
    callbackUrl,
    notifyUrl,
    reference1,
    reference2,
    reference3,
    remark,
    expiryAt
  }

  transactionModel.create(transaction, { transaction: t })
    .then((transaction) => {
      req.transaction = transaction
    })
    .then(() => next())
    .catch((err) => next(err))
}

exports.findTransactions = (options) => (req, res, next) => {
  const partnerId = req.headers['x-partner-id']
  const timezone = req.headers['x-timezone']
  const email = req.headers['x-email']

  const isExport = req.query.is_export

  const {
    q,
    type,
    sub_type: subType,
    order_id: orderId,
    reference_no: referenceNo,
    user_uid: userUid,
    wallet_id: walletId,
    status,
    date,
    start_date: startDate,
    end_date: endDate
  } = req.query

  const offset = req.query.offset ? Number(req.query.offset) : 0
  const limit = req.query.limit ? Number(req.query.limit) : 10

  const query = {
    include: [
      {
        model: partnerModel,
        as: 'partner',
        required: true
      },
      {
        model: masterTransactionTypeModel,
        as: 'type',
        required: true
      },
      {
        model: masterTransactionSubTypeModel,
        as: 'subType',
        required: true
      },
      {
        model: masterBankModel,
        as: 'bank',
        required: false
      }
    ],
    where: {},
    order: [['createdAt', 'DESC']],
    offset: offset,
    limit: isExport !== 'true' ? limit : undefined
  }

  if (options && options.isExcludePending) {
    query.where.status = {
      [Op.ne]: transactionStatusMaster.PENDING
    }
  }

  if (partnerId) query.where.partnerId = partnerId
  if (q) {
    query.where[Op.or] = [
      { referenceNo: q },
      { orderId: q },
      { payerName: { [Op.like]: `%${q}%` } },
      { payeeName: { [Op.like]: `%${q}%` } },
      { batchNo: q }
    ]
  }
  if (type) query.where.transactionTypeId = transactionTypeMaster[type].id
  if (subType) query.where.transactionSubTypeId = transactionSubTypeMaster[subType].id
  if (orderId) query.where.orderId = orderId
  if (referenceNo) query.where.referenceNo = referenceNo
  if (userUid) {
    query.where[Op.or] = [
      { payerUid: userUid },
      { payeeUid: userUid }
    ]
  }
  if (walletId) {
    query.where[Op.or] = [
      { payerWalletId: walletId },
      { payeeWalletId: walletId }
    ]
  }
  if (status) query.where.status = status
  if (date) {
    query.where.createdAt = {
      [Op.between]: [
        dayjs.tz(date, timezone).startOf('day').utc().format(),
        dayjs.tz(date, timezone).endOf('day').utc().format()
      ]
    }
  }
  if (startDate && endDate) {
    query.where.createdAt = {
      [Op.between]: [
        dayjs.tz(startDate, timezone).startOf('day').utc().format(),
        dayjs.tz(endDate, timezone).endOf('day').utc().format()
      ]
    }
  }

  transactionModel.count(query)
    .then((count) => {
      if (isExport && count > 1000) {
        const transactionExportQueue = new Queue('TRANSACTIONS_EXPORT', { redis: { host: REDIS_HOST, port: REDIS_PORT } })
        transactionExportQueue.add({
          partnerId,
          query: {
            q,
            type,
            subType,
            status,
            startDate: dayjs.tz(startDate, timezone).startOf('day').utc().format(),
            endDate: dayjs.tz(endDate, timezone).endOf('day').utc().format()
          },
          email
        })
        throw new ServiceError(transactionError.ERR_TRANASACTION_EXPORT_LIMIT)
      } else {
        req.total = count
      }
    })
    .then(() => transactionModel.findAll(query))
    .then((transactions) => {
      req.transactions = transactions
    })
    .then(() => next())
    .catch((err) => next(err))
}

exports.findTransaction = (proxyType, proxyValue) => (req, res, next) => {
  const t = req.t
  const partnerId = req.headers['x-partner-id']

  const query = {
    include: [
      {
        model: partnerModel,
        as: 'partner',
        required: true
      },
      {
        model: masterTransactionTypeModel,
        as: 'type',
        required: true
      },
      {
        model: masterTransactionSubTypeModel,
        as: 'subType',
        required: true
      },
      {
        model: masterBankModel,
        as: 'bank',
        required: false
      }
    ],
    where: {},
    ...t ? {
      transaction: t,
      lock: t.LOCK.UPDATE
    } : {}
  }

  if (partnerId) query.where.partnerId = partnerId
  switch (proxyType) {
    case 'id':
      switch (proxyValue) {
        case 'param':
          query.where.id = req.params.id
          break
      }
      break
    case 'orderId':
      switch (proxyValue) {
        case 'body.data':
          query.where.referenceNo = req.body.data.order_id
          break
      }
      break
    case 'referenceNo':
      switch (proxyValue) {
        case 'body':
          query.where.referenceNo = req.body.reference_no
          break
      }
      break
  }
  transactionModel.findOne(query)
    .then((transaction) => {
      if (!transaction) {
        throw new ServiceError(transactionError.ERR_TRANSACTION_NOT_FOUND)
      } else {
        req.transaction = transaction
      }
    })
    .then(() => next())
    .catch((err) => next(err))
}

exports.validateLimitPerTransaction = (transactionType, transactionSubType) => (req, res, next) => {
  const t = req.t
  const partnerId = req.headers['x-partner-id']
  const amount = Number(req.body.amount)

  const query = {
    where: {
      partnerId,
      transactionTypeId: transactionType.id,
      transactionSubTypeId: transactionSubType.id
    },
    transaction: t
  }

  transactionLimitModel.findOne(query)
    .then((transactionLimit) => {
      if (transactionLimit && Number(transactionLimit.limitPerTransaction) !== 0) {
        if (amount > Number(transactionLimit.limitPerTransaction)) {
          throw new ServiceError(transactionError.ERR_TRANSACTION_LIMIT_AMOUNT_PER_TRANSACTION)
        }
      }
    })
    .then(() => next())
    .catch((err) => next(err))
}

exports.findWalletSummaryPerDay = (walletType, role) => (req, res, next) => {
  const t = req.t
  const partnerId = req.headers['x-partner-id'] || req.transaction.partnerId
  const timezone = req.headers['x-timezone']

  let walletId
  switch (role) {
    case 'payer':
      if (req.payer) walletId = req.payer.wallets.find((wallet) => wallet.walletTypeId === walletType.id).walletId
      else walletId = req.transaction.payerWalletId
      break
    case 'payee':
      if (req.payee) walletId = req.payee.wallets.find((wallet) => wallet.walletTypeId === walletType.id).walletId
      else walletId = req.transaction.payeeWalletId
      break
  }

  const query = {
    attributes: [
      'transactionTypeId',
      'transactionSubTypeId',
      [Sequelize.fn('SUM', Sequelize.literal(`
        CASE
          WHEN transactionTypeId = ${transactionTypeMaster.TOPUP.id}
            THEN CASE WHEN payeeWalletId = ${walletId} THEN net ELSE 0 END
          WHEN transactionTypeId = ${transactionTypeMaster.PAYMENT.id}
            THEN CASE WHEN payerWalletId = ${walletId} THEN net ELSE 0 END
          WHEN transactionTypeId = ${transactionTypeMaster.TRANSFER.id}
            THEN CASE WHEN payerWalletId = ${walletId} THEN net ELSE 0 END
          WHEN transactionTypeId = ${transactionTypeMaster.WITHDRAW.id}
            THEN CASE WHEN payerWalletId = ${walletId} THEN net ELSE 0 END
          ELSE 0
        END
      `)), 'sum']
    ],
    where: {
      partnerId,
      status: transactionStatusMaster.APPROVED,
      approvedAt: {
        [Op.between]: [
          dayjs().tz(timezone).startOf('day').utc().format(),
          dayjs().tz(timezone).endOf('day').utc().format()
        ]
      }
    },
    group: ['transactionTypeId', 'transactionSubTypeId'],
    transaction: t
  }

  transactionModel.findAll(query)
    .then((summary) => {
      switch (role) {
        case 'payer':
          if (req.payer) req.payer.summary = summary
          else req.payer = { summary }
          break
        case 'payee':
          if (req.payee) req.payee.summary = summary
          else req.payee = { summary }
          break
      }
    })
    .then(() => next())
    .catch((err) => next(err))
}

exports.validateLimitPerDay = (transactionType, transactionSubType, role) => (req, res, next) => {
  const t = req.t
  const partnerId = req.headers['x-partner-id']

  let summary
  switch (role) {
    case 'payer':
      summary = req.payer.summary.find((_summary) => _summary.transactionTypeId === transactionType.id && _summary.transactionSubTypeId === transactionSubType.id)
      break
    case 'payee':
      summary = req.payee.summary.find((_summary) => _summary.transactionTypeId === transactionType.id && _summary.transactionSubTypeId === transactionSubType.id)
      break
  }

  if (summary) {
    const sum = Number(summary.get('sum'))
    const query = {
      where: {
        partnerId,
        transactionTypeId: transactionType.id,
        transactionSubTypeId: transactionSubType.id
      },
      transaction: t
    }
    transactionLimitModel.findOne(query)
      .then((transactionLimit) => {
        if (transactionLimit && Number(transactionLimit.limitPerDay) > 0) {
          switch (role) {
            case 'payer': {
              if (sum >= Number(transactionLimit.limitPerDay)) {
                throw new ServiceError(transactionError.ERR_TRANSACTION_PAYER_LIMIT_AMOUNT_PER_DAY)
              }
              break
            }
            case 'payee': {
              if (sum >= Number(transactionLimit.limitPerDay)) {
                throw new ServiceError(transactionError.ERR_TRANSACTION_PAYEE_LIMIT_AMOUNT_PER_DAY)
              }
              break
            }
          }
        }
      })
      .then(() => next())
      .catch((err) => next(err))
  } else {
    next()
  }
}

exports.validatePayerBalance = (walletType) => (req, res, next) => {
  const net = Number(req.body.amount)
  const fee = req.fee ? Number(req.fee) : 0
  const discount = req.body.discount ? Number(req.body.discount) : 0
  const total = net + fee - discount

  const wallet = req.payer.wallets.find((wallet) => wallet.walletTypeId === walletType.id)
  if (Number(wallet.balance) < total) {
    next(new ServiceError(transactionError.ERR_TRANSACTION_PAYER_INSUFFICIENT_BALANCE))
  } else {
    next()
  }
}

exports.approveTransaction = () => async (req, res, next) => {
  const t = req.t
  const transactionConfig = req.transactionConfig
  const transaction = req.transaction

  const transactedAt = dayjs.utc()

  switch (transaction.status) {
    case transactionStatusMaster.HOLDED:
      return next(new ServiceError(transactionError.ERR_TRANSACTION_ALREADY_HOLDED))
    case transactionStatusMaster.APPROVED:
      return next(new ServiceError(transactionError.ERR_TRANSACTION_ALEADY_APPROVED))
    case transactionStatusMaster.CANCELLED:
      return next(new ServiceError(transactionError.ERR_TRANSACTION_ALEADY_CANCELLED))
    case transactionStatusMaster.FAILED:
      return next(new ServiceError(transactionError.ERR_TRANSACTION_ALEADY_FAILED))
    case transactionStatusMaster.VOIDED:
      return next(new ServiceError(transactionError.ERR_TRANSACTION_ALEADY_VOIDED))
    case transactionStatusMaster.REFUNDED:
      return next(new ServiceError(transactionError.ERR_TRANSACTION_ALEADY_REFUNDED))
    case transactionStatusMaster.SETTLED:
      return next(new ServiceError(transactionError.ERR_TRANSACTION_ALEADY_SETTLED))
  }

  if (transaction.expiryAt && transactedAt.isAfter(dayjs.utc(transaction.expiryAt))) {
    await transaction.update({
      failedReason: 'Transaction Expired',
      status: transactionStatusMaster.FAILED,
      failedAt: transactedAt.format()
    }, { transaction: t })
    return next(new ServiceError(transactionError.ERR_TRANSACTION_ALREADY_EXPIRED))
  }

  const update = {}
  if (transactionConfig && transactionConfig.isHolding) {
    update.status = transactionStatusMaster.HOLDED
    update.holdedAt = transactedAt.format()
  } else {
    update.approvalCode = req.approvalCode || customAlphabet(dictionary.numbers, 6)()
    update.status = transactionStatusMaster.APPROVED
    update.approvedAt = transactedAt.format()
  }

  try {
    const payerWallet = await walletModel.findOne({
      where: {
        partnerId: transaction.partnerId,
        walletId: transaction.payerWalletId
      },
      transaction: t
    })
    const payeeWallet = await walletModel.findOne({
      where: {
        partnerId: transaction.partnerId,
        walletId: transaction.payeeWalletId
      },
      transaction: t
    })
    const walletLimit = await walletLimitModel.findOne({
      where: {
        partnerId: transaction.partnerId,
        walletTypeId: payeeWallet.walletTypeId
      },
      transaction: t
    })

    if (
      payerWallet.walletTypeId === walletTypeMaster.CUSTOMER.id &&
      Number(payerWallet.balance) < Number(transaction.total)
    ) {
      throw new ServiceError(transactionError.ERR_TRANSACTION_PAYER_INSUFFICIENT_BALANCE)
    } else if (
      walletLimit &&
      Number(walletLimit.limitBalance) > 0 &&
      Number(payeeWallet.balance) +
      Number(payeeWallet.holdBalance) +
      Number(transaction.net) > Number(walletLimit.limitBalance)
    ) {
      throw new ServiceError(transactionError.ERR_TRANSACTION_PAYEE_MAXIMUM_BALANCE_EXCEEDED)
    } else {
      if (Number(transaction.fee) > 0) {
        await walletModel.increment('balance', {
          by: transaction.fee,
          where: {
            partnerId: transaction.partnerId,
            walletId: transaction.feeExudeWalletId
          },
          transaction: t
        })
      }

      if (Number(transaction.discount) > 0) {
        await walletModel.decrement('balance', {
          by: transaction.discount,
          where: {
            partnerId: transaction.partnerId,
            walletId: transaction.discountAbsorbWalletId
          },
          transaction: t
        })
      }

      await userModel.update({ recentTransactedAt: transactedAt.format() }, {
        where: { id: transaction.payerId },
        transaction: t
      })

      await userModel.update({ recentTransactedAt: transactedAt.format() }, {
        where: { id: transaction.payeeId },
        transaction: t
      })

      if (transaction.transactionTypeId === transactionTypeMaster.TOPUP.id) {
        await payerWallet.decrement('balance', {
          by: transaction.net,
          transaction: t
        })
      } else {
        await payerWallet.decrement('balance', {
          by: transaction.total,
          transaction: t
        })
      }

      if (update.status === transactionStatusMaster.HOLDED) {
        await payeeWallet.increment('holdBalance', {
          by: transaction.net,
          transaction: t
        })
      } else {
        await payeeWallet.increment('balance', {
          by: transaction.net,
          transaction: t
        })
      }

      await transaction.update(update, { transaction: t })
      next()
    }
  } catch (err) {
    next(err)
  }
}

exports.approveHoldedTransaction = () => async (req, res, next) => {
  const t = req.t
  const transaction = req.transaction

  const approvalCode = customAlphabet(dictionary.numbers, 6)()

  switch (transaction.status) {
    case transactionStatusMaster.APPROVED:
      return next(new ServiceError(transactionError.ERR_TRANSACTION_ALEADY_APPROVED))
    case transactionStatusMaster.CANCELLED:
      return next(new ServiceError(transactionError.ERR_TRANSACTION_ALEADY_CANCELLED))
    case transactionStatusMaster.FAILED:
      return next(new ServiceError(transactionError.ERR_TRANSACTION_ALEADY_FAILED))
    case transactionStatusMaster.VOIDED:
      return next(new ServiceError(transactionError.ERR_TRANSACTION_ALEADY_VOIDED))
    case transactionStatusMaster.REFUNDED:
      return next(new ServiceError(transactionError.ERR_TRANSACTION_ALEADY_REFUNDED))
    case transactionStatusMaster.SETTLED:
      return next(new ServiceError(transactionError.ERR_TRANSACTION_ALEADY_SETTLED))
  }

  try {
    const payeeWallet = await walletModel.findOne({
      where: {
        partnerId: transaction.partnerId,
        walletId: transaction.payeeWalletId
      },
      transaction: t
    })

    await transaction.update({
      approvalCode: approvalCode,
      status: transactionStatusMaster.APPROVED,
      approvedAt: dayjs.utc().format()
    }, { transaction: t })
    await payeeWallet.decrement('holdBalance', {
      by: transaction.net,
      transaction: t
    })
    await payeeWallet.increment('balance', {
      by: transaction.net,
      transaction: t
    })
    next()
  } catch (err) {
    next(err)
  }
}

exports.voidTransaction = (options) => async (req, res, next) => {
  const t = req.t
  const transaction = req.transaction

  const timezone = req.headers['x-timezone']
  const {
    operator_id: operatorId,
    operator_name: operatorName,
    reason
  } = req.body

  const schema = Joi.object({
    transactionTypeId: Joi.number()
      .valid(
        transactionTypeMaster.TOPUP.id,
        transactionTypeMaster.PAYMENT.id
      )
      .error(() => new ServiceError(transactionError.ERR_TRANSACTION_TYPE_NOT_ALLOWED_TO_VOID)),
    transactionSubTypeId: Joi.number()
      .when('transactionTypeId', {
        is: transactionTypeMaster.TOPUP.id,
        then: Joi.number().valid(transactionSubTypeMaster.CREDITCARD.id)
      })
      .when('transactionTypeId', {
        is: transactionTypeMaster.PAYMENT.id,
        then: Joi.number().valid(transactionSubTypeMaster.CLOSELOOP.id)
      })
      .error(() => new ServiceError(transactionError.ERR_TRANSACTION_TYPE_NOT_ALLOWED_TO_VOID)),
    status: Joi.string()
      .valid(transactionStatusMaster.APPROVED)
      .error(() => new ServiceError(transactionError.ERR_TRANSACTION_STATUS_NOT_ALLOWED_TO_VOID)),
    ...options && options.isCheckDate === false ? {} : {
      createdAt: Joi.date()
        .iso()
        .greater(dayjs().tz(timezone).startOf('day').utc().toDate())
        .less(dayjs().tz(timezone).endOf('day').utc().toDate())
        .error(() => new ServiceError(transactionError.ERR_TRANSACTION_CAN_ONLY_BE_VOIDED_ON_THE_SAME_DAY_AS_THE_DATE_OF_TRANSACT))
    }
  }).unknown(true)

  try {
    await schema.validateAsync(transaction)
    const payerWallet = await walletModel.findOne({
      where: {
        partnerId: transaction.partnerId,
        walletId: transaction.payerWalletId
      },
      transaction: t
    })
    const payeeWallet = await walletModel.findOne({
      where: {
        partnerId: transaction.partnerId,
        walletId: transaction.payeeWalletId
      },
      transaction: t
    })

    if (Number(payeeWallet.balance) < Number(transaction.net)) {
      throw new ServiceError(transactionError.ERR_TRANSACTION_PAYEE_BALANCE_INSUFFICIENT_TO_VOID)
    }

    if (transaction.feeExudeWalletId) {
      const feeExudeWallet = await walletModel.findOne({
        where: {
          partnerId: transaction.partnerId,
          walletId: transaction.feeExudeWalletId
        },
        transaction: t
      })
      feeExudeWallet.decrement('balance', { by: transaction.fee })
    }

    if (transaction.discountAbsorbWalletId) {
      const discountAbsorbWallet = await walletModel.findOne({
        where: {
          partnerId: transaction.partnerId,
          walletId: transaction.discountAbsorbWalletId
        },
        transaction: t
      })
      discountAbsorbWallet.increment('balance', { by: transaction.discount, transaction: t })
    }

    await payerWallet.increment('balance', { by: transaction.total, transaction: t })
    await payeeWallet.decrement('balance', { by: transaction.net, transaction: t })
    await transaction.update({
      status: transactionStatusMaster.VOIDED,
      remark: reason,
      voidedById: operatorId,
      voidedByName: operatorName,
      voidedAt: dayjs.utc().format()
    }, { transaction: t })

    const transactionSummary = await transactionSummaryModel.findOne({
      where: {
        partnerId: transaction.partnerId,
        transactionTypeId: transaction.transactionTypeId,
        transactionSubTypeId: transaction.transactionSubTypeId
      },
      transaction: t
    })
    transactionSummary.decrement(['daily', 'weekly', 'monthly', 'yearly'], {
      by: transaction.total,
      transaction: t
    })
    next()
  } catch (err) {
    next(err)
  }
}
