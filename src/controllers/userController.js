const userError = require('../constants/errors/userError')

const userProxyMaster = require('../constants/masters/userProxyMaster')
const userStatusMaster = require('../constants/masters/userStatusMaster')
const userTypeMaster = require('../constants/masters/userTypeMaster')

const { ServiceError } = require('../helpers/error')

const userModel = require('../models/userModel')
const walletModel = require('../models/walletModel')

exports.findUser = (proxyType, proxyValue, userType, userSubType, role) => (req, res, next) => {
  const partnerId = req.headers['x-partner-id']

  const query = {
    include: [
      {
        model: walletModel,
        as: 'wallets',
        where: {},
        required: true
      }
    ],
    where: {}
  }

  if (userType) {
    query.where.userTypeId = userType.id

    if (userType === userTypeMaster.PARTNER) {
      query.where.partnerId = partnerId
    }
  }
  if (userSubType) query.where.userSubTypeId = userSubType.id

  switch (proxyType) {
    case 'walletId':
      switch (proxyValue) {
        case 'transaction':
          switch (role) {
            case 'payer':
              query.include[query.include.findIndex((include) => include.model === walletModel)].where.walletId = req.transaction.payerWalletId
              break
            case 'payee':
              query.include[query.include.findIndex((include) => include.model === walletModel)].where.walletId = req.transaction.payeeWalletId
              break
          }
          break
        case 'body':
          query.include[query.include.findIndex((include) => include.model === walletModel)].where.walletId = req.body.wallet_id
          break
      }
      break
    case 'payer':
      switch (proxyValue) {
        case 'body':
          switch (req.body.payer_proxy_type) {
            case userProxyMaster.WALLETID:
              query.include[query.include.findIndex((include) => include.model === walletModel)].where.walletId = req.body.payer_proxy_value
              break
          }
          break
      }
      break
    case 'payee':
      switch (proxyValue) {
        case 'body':
          switch (req.body.payee_proxy_type) {
            case userProxyMaster.WALLETID:
              query.include[query.include.findIndex((include) => include.model === walletModel)].where.walletId = req.body.payee_proxy_value
              break
          }
          break
      }
      break
  }

  userModel.findOne(query)
    .then((user) => {
      if (!user) {
        throw new ServiceError(userError.ERR_USER_NOT_FOUND)
      } else if (
        user.userTypeId !== userTypeMaster.SYSTEM.id &&
        user.partnerId !== Number(partnerId)
      ) {
        throw new ServiceError(userError.ERR_USER_NOT_FOUND)
      } else {
        if (user.status === userStatusMaster.SUSPENDED) {
          throw new ServiceError(userError.ERR_USER_SUSPENDED)
        } else {
          switch (role) {
            case 'system':
              req.system = user
              break
            case 'partner':
              req.partner = user
              break
            case 'payer':
              req.payer = user
              break
            case 'payee':
              req.payee = user
              break
          }
        }
      }
    })
    .then(() => next())
    .catch((err) => next(err))
}
