const Sequelize = require('sequelize')

const transactionFeeModel = require('../models/transactionFeeModel')

const Op = Sequelize.Op

exports.findFee = (transactionType, transactionSubType) => (req, res, next) => {
  const t = req.t
  const partnerId = req.headers['x-partner-id']
  const {
    fee,
    amount
  } = req.body

  if (fee) {
    req.fee = fee
    next()
  } else {
    const query = {
      where: {
        partnerId,
        transactionTypeId: transactionType.id,
        transactionSubTypeId: transactionSubType.id,
        start: { [Op.lte]: amount },
        end: { [Op.gte]: amount }
      },
      transaction: t
    }
    transactionFeeModel.findOne(query)
      .then((fee) => {
        if (fee) {
          req.fee = Number(fee.fee)
        } else {
          req.fee = 0
        }
      })
      .then(() => next())
      .catch((err) => next(err))
  }
}
