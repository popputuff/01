const os = require('os')
const crypto = require('crypto')

const axios = require('axios')
const dayjs = require('dayjs')

dayjs.extend(require('dayjs/plugin/utc'))

const { ServiceError } = require('../helpers/error')

const octopusServiceError = require('../constants/errors/octopusServiceError')

const octopusTransactionStatusMaster = require('../constants/masters/octopusTransactionStatusMaster')
const transactionStatusMaster = require('../constants/masters/transactionStatusMaster')
const transactionSubTypeMaster = require('../constants/masters/transactionSubTypeMaster')
const transactionTypeMaster = require('../constants/masters/transactionTypeMaster')

const transactionController = require('../controllers/transactionController')
const transactionSummaryController = require('../controllers/transactionSummaryController')
const notificationController = require('../controllers/notificationController')

const octopusCredentialModel = require('../models/octopusCredentialModel')
const octopusExchangeLogModel = require('../models/octopusExchangeLogModel')
const octopusPaymentProfileModel = require('../models/octopusPaymentProfileModel')

const SERVICE_NOTIFY = process.env.SERVICE_NOTIFY
const SERVICE_OCTOPUS = process.env.SERVICE_OCTOPUS

const instance = axios.create({
  baseURL: SERVICE_OCTOPUS,
  timeout: 20000
})

instance.interceptors.request.use(async (config) => {
  const octopusExchangeLog = {
    partnerId: config.partnerId,
    requestId: config.requestId,
    exchange: 'request',
    path: config.url,
    source: 'system',
    sourceHost: os.hostname(),
    destination: 'octopus',
    destinationHost: new URL(config.baseURL).hostname,
    headers: JSON.stringify(config.headers),
    body: JSON.stringify(config.data)
  }
  await octopusExchangeLogModel.create(octopusExchangeLog)
  return config
}, (err) => Promise.reject(err))

instance.interceptors.response.use(async (response) => {
  const octopusExchangeLog = {
    partnerId: response.config.partnerId,
    requestId: response.config.requestId,
    exchange: 'response',
    path: response.config.url,
    source: 'octopus',
    sourceHost: response.request.socket.servername,
    destination: 'system',
    destinationHost: os.hostname(),
    statusCode: response.status,
    headers: JSON.stringify(response.headers),
    body: JSON.stringify(response.data)
  }
  await octopusExchangeLogModel.create(octopusExchangeLog)
  return response
}, async (err) => {
  if (err.response) {
    const octopusExchangeLog = {
      partnerId: err.response.config.partnerId,
      requestId: err.response.config.requestId,
      exchange: 'response',
      path: err.response.config.url,
      source: 'octopus',
      sourceHost: err.response.request.socket.servername,
      destination: 'system',
      destinationHost: os.hostname(),
      statusCode: err.response.status,
      headers: JSON.stringify(err.response.headers),
      body: JSON.stringify(err.response.data)
    }
    return octopusExchangeLogModel.create(octopusExchangeLog)
      .then(() => Promise.reject(new ServiceError(octopusServiceError.ERR_OCTOPUS_SERVICE_INTERNAL_SERVER_ERROR)))
      .catch((err) => Promise.reject(err))
  } else {
    return Promise.reject(err)
  }
})

exports.findOctopusCredential = (proxyType, proxyValue) => (req, res, next) => {
  const t = req.t
  const query = {
    where: {},
    transaction: t
  }
  switch (proxyType) {
    case 'partnerId':
      switch (proxyValue) {
        case 'header':
          query.where.partnerId = req.headers['x-partner-id']
          break
        case 'transaction':
          query.where.partnerId = req.transaction.partnerId
          break
      }
      break
  }

  octopusCredentialModel.findOne(query)
    .then((octopusCredential) => {
      req.octopusCredential = octopusCredential
    })
    .then(() => next())
    .catch((err) => next(err))
}

exports.findOctopusPaymentProfile = (proxyType, proxyValue) => (req, res, next) => {
  const t = req.t
  const transaction = req.transaction

  const query = {
    where: {
      transactionTypeId: transaction.transactionTypeId,
      transactionSubTypeId: transaction.transactionSubTypeId
    },
    transaction: t
  }

  switch (proxyType) {
    case 'partnerId':
      switch (proxyValue) {
        case 'header':
          query.where.partnerId = req.headers['x-partner-id']
          break
        case 'transaction':
          query.where.partnerId = req.transaction.partnerId
          break
      }
      break
  }

  octopusPaymentProfileModel.findOne(query)
    .then((octopusPaymentProfile) => {
      req.octopusPaymentProfile = octopusPaymentProfile
    })
    .then(() => next())
    .catch((err) => next(err))
}

exports.validateSignData = () => (req, res, next) => {
  const octopusCredential = req.octopusCredential

  const hmac = crypto.createHmac('sha256', octopusCredential.signKey)
  const sign = hmac.update(JSON.stringify(req.body.data)).digest('base64')

  if (sign !== req.body.sign) {
    next(new ServiceError(octopusServiceError.ERR_OCTOPUS_SERVICE_INVALID_SIGN_DATA))
  } else {
    next()
  }
}

exports.executeNotifyTransaction = () => (req, res, next) => {
  const t = req.t
  const transaction = req.transaction

  const {
    status,
    failed_reason: failedReason,
    approval_code: approvalCode
  } = req.body.data

  if (transaction.status !== transactionStatusMaster.PENDING) {
    throw new ServiceError(octopusServiceError.ERR_OCTOPUS_SERVICE_TRANSACTION_NOT_PENDING)
  } else {
    if (status === octopusTransactionStatusMaster.APPROVED) {
      req.approvalCode = approvalCode
      next()
    } else {
      transaction.update({
        failedReason,
        status: transactionStatusMaster.FAILED,
        failedAt: dayjs.utc().format()
      }, { transaction: t })
        .then(() => {
          t.commit()
          req.t = undefined
          throw new ServiceError(octopusServiceError.ERR_OCTOPUS_SERVICE_TRANSACTION_FAILED)
        })
        .catch((err) => next(err))
    }
  }
}

exports.createPayment = (paymentType) => (req, res, next) => {
  const requestId = req.requestId

  const t = req.t
  const payee = req.payee
  const transaction = req.transaction
  const octopusCredential = req.octopusCredential
  const octopusPaymentProfile = req.octopusPaymentProfile

  const partnerId = octopusPaymentProfile.uid
  const amount = Number(transaction.total)
  const urlNotify = `${SERVICE_NOTIFY}/${partnerId}`
  const urlRedirect = process.env.OCTOPUT_REDIRECT_URL
  const signKey = octopusCredential.signKey
  const payload = {
    mid: octopusPaymentProfile.imid,
    order_id: transaction.referenceNo,
    amount,
    urlRedirect,
    urlNotify,
    customer_email: payee.email
  }
  const hmac = crypto.createHmac('sha256', Buffer.from(signKey, 'base64'))
  const contentSignature = hmac.update(JSON.stringify(payload)).digest('base64')

  instance.request({
    method: 'POST',
    url: '/v2/payment',
    headers: {
      'x-api-id': octopusCredential.apiId,
      'x-api-key': octopusCredential.apiKey,
      'x-partner-id': partnerId,
      'x-content-signature': contentSignature
    },
    data: payload,
    partnerId: partnerId,
    requestId: requestId
  })
    .then((response) => {
      console.log(response)
      const redirectUrl = response.data.redirect_url
      const referenceNo = response.data.reference
      req.redirectUrl = redirectUrl
      return transaction.update({ externalReference: referenceNo }, { transaction: t })
    })
    .then(() => next())
    .catch((err) => next(err))
}

exports.inquiryTransaction = () => (req, res, next) => {
  const requestId = req.requestId

  const transaction = req.transaction
  const octopusCredential = req.octopusCredential
  const octopusPaymentProfile = req.octopusPaymentProfile

  const partnerId = req.headers['x-partner-id']

  instance.request({
    method: 'POST',
    url: '/api/v2/direct/inquiry',
    headers: {
      'x-api-id': octopusCredential.apiId,
      'x-api-key': octopusCredential.apiKey
    },
    data: {
      merchant_id: octopusPaymentProfile.imid,
      order_id: transaction.referenceNo
    },
    partnerId: partnerId,
    requestId: requestId
  })
    .then(async ({ data }) => {
      if (data.data.status !== transaction.status) {
        switch (data.data.status) {
          case transactionStatusMaster.APPROVED:
            return new Promise((resolve, reject) => {
              transactionController.approveTransaction()(req, res, (err) => {
                if (err) reject(err)
                else transactionSummaryController.incrementTransactionSummary()(req, res, (err) => err ? reject(err) : resolve(true))
              })
            })
          case transactionStatusMaster.VOIDED:
            return new Promise((resolve, reject) => {
              transactionController.voidTransaction({ isCheckDate: false })(req, res, (err) => {
                if (err) reject(err)
                else transactionSummaryController.decrementTransactionSummary()(req, res, (err) => err ? reject(err) : resolve(true))
              })
            })
        }
      }
    })
    .then((isSendNotification) => {
      if (isSendNotification) {
        return new Promise((resolve, reject) => {
          notificationController.sendNotification()(req, res, (err) => err ? reject(err) : resolve())
        })
      }
    })
    .then(() => next())
    .catch((err) => next(err))
}

exports.voidTransaction = () => (req, res, next) => {
  const requestId = req.requestId

  const transaction = req.transaction
  const octopusCredential = req.octopusCredential
  const octopusPaymentProfile = req.octopusPaymentProfile

  const partnerId = req.headers['x-partner-id']
  const reason = req.body.reason

  if (
    transaction.transactionTypeID !== transactionTypeMaster.TOPUP.id &&
    transaction.transactionSubTypeId !== transactionSubTypeMaster.CREDITCARD.id
  ) {
    next()
  } else {
    instance.request({
      method: 'POST',
      url: '/api/v2/direct/void',
      headers: {
        'x-api-id': octopusCredential.apiId,
        'x-api-key': octopusCredential.apiKey
      },
      data: {
        merchant_id: octopusPaymentProfile.imid,
        reference: transaction.externalReference,
        reason: reason
      },
      partnerId: partnerId,
      requestId: requestId
    })
      .then(() => next())
      .catch((err) => next(err))
  }
}
