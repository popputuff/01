const express = require('express')
const dayjs = require('dayjs')

const sequelize = require('./helpers/sequelize')

const adjustmentRouter = require('./routers/adjustmentRouter')
const historyRouter = require('./routers/historyRouter')
const notifyRouter = require('./routers/notifyRouter')
const paymentRouter = require('./routers/paymentRouter')
const topupRouter = require('./routers/topupRouter')
const transactionRouter = require('./routers/transactionRouter')
const transferRouter = require('./routers/transferRouter')
const withdrawRouter = require('./routers/withdrawRouter')
const settlementRouter = require('./routers/settlementRouter')

const router = express.Router()
const serviceStart = dayjs()

const ENVIRONMENT = process.env.ENVIRONMENT

router.get('/', async (req, res, next) => {
  const databaseStatus = await sequelize.authenticate()
    .then(() => 'Connected')
    .catch(() => 'Disconnected')

  res.json({
    requestUID: req.requestId,
    service: 'Whalepay Transaction Service',
    environment: ENVIRONMENT,
    start_date: serviceStart.utc().toString(),
    current_date: dayjs.utc().toString(),
    database: databaseStatus
  })
  next()
})

router.use('/adjustment', adjustmentRouter)
router.use('/history', historyRouter)
router.use('/notify', notifyRouter)
router.use('/payment', paymentRouter)
router.use('/topup', topupRouter)
router.use('/transaction', transactionRouter)
router.use('/transfer', transferRouter)
router.use('/withdraw', withdrawRouter)
router.use('/settlement', settlementRouter)

module.exports = router
