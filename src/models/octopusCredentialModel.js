const { DataTypes } = require('sequelize')

const sequelize = require('../helpers/sequelize')

const octopusCredentialModel = sequelize.define('WP_OCTOPUS_CREDENTIAL', {
  id: {
    type: DataTypes.INTEGER(11),
    primaryKey: true,
    allowNull: false,
    autoIncrement: true
  },
  partnerId: {
    type: DataTypes.INTEGER(11),
    allowNull: false
  },
  apiId: {
    type: DataTypes.STRING(100),
    allowNull: false
  },
  apiKey: {
    type: DataTypes.STRING(100),
    allowNull: false
  },
  signKey: {
    type: DataTypes.STRING(100),
    allowNull: false
  }
})

module.exports = octopusCredentialModel
