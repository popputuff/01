const { DataTypes } = require('sequelize')

const sequelize = require('../helpers/sequelize')

const octopusPaymentProfileModel = sequelize.define('WP_OCTOPUS_PAYMENT_PROFILE', {
  id: {
    type: DataTypes.INTEGER(11),
    primaryKey: true,
    allowNull: false,
    autoIncrement: true
  },
  partnerId: {
    type: DataTypes.INTEGER(11),
    allowNull: false
  },
  transactionTypeId: {
    type: DataTypes.INTEGER(11),
    allowNull: false
  },
  transactionSubTypeId: {
    type: DataTypes.INTEGER(11),
    allowNull: false
  },
  uid: {
    type: DataTypes.STRING(10),
    allowNull: false
  },
  imid: {
    type: DataTypes.STRING(20),
    allowNull: false
  }
})

module.exports = octopusPaymentProfileModel
