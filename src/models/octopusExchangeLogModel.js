const { DataTypes } = require('sequelize')

const sequelize = require('../helpers/sequelize')

const octopusExchangeLogModel = sequelize.define('WP_OCTOPUS_EXCHANGE_LOG', {
  id: {
    type: DataTypes.INTEGER(11),
    primaryKey: true,
    allowNull: false,
    autoIncrement: true
  },
  partnerId: {
    type: DataTypes.INTEGER(11),
    allowNull: false
  },
  requestId: {
    type: DataTypes.STRING(21),
    allowNull: false
  },
  exchange: {
    type: DataTypes.ENUM('request', 'response'),
    allowNull: false
  },
  path: {
    type: DataTypes.STRING(255),
    allowNull: false
  },
  source: {
    type: DataTypes.ENUM('system', 'octopus'),
    allowNull: false
  },
  sourceHost: {
    type: DataTypes.STRING(255),
    allowNull: false
  },
  destination: {
    type: DataTypes.ENUM('system', 'octopus'),
    allowNull: false
  },
  destinationHost: {
    type: DataTypes.STRING(255),
    allowNull: false
  },
  statusCode: {
    type: DataTypes.STRING(3)
  },
  headers: {
    type: DataTypes.TEXT('long')
  },
  body: {
    type: DataTypes.TEXT('long')
  }
})

module.exports = octopusExchangeLogModel
