const { DataTypes } = require('sequelize')

const sequelize = require('../helpers/sequelize')

const transactionConfigModel = sequelize.define('WP_TRANSACTION_CONFIG', {
  id: {
    type: DataTypes.INTEGER(11),
    primaryKey: true,
    allowNull: false,
    autoIncrement: true
  },
  partnerId: {
    type: DataTypes.INTEGER(11),
    allowNull: false
  },
  transactionTypeId: {
    type: DataTypes.INTEGER(11),
    allowNull: false
  },
  transactionSubTypeId: {
    type: DataTypes.INTEGER(11),
    allowNull: false
  },
  isHolding: {
    type: DataTypes.BOOLEAN(),
    allowNull: false
  }
})

module.exports = transactionConfigModel
