const { DataTypes } = require('sequelize')

const sequelize = require('../helpers/sequelize')

const partnerModel = sequelize.define('WP_PARTNER', {
  id: {
    type: DataTypes.INTEGER(11),
    primaryKey: true,
    allowNull: false,
    autoIncrement: true
  },
  name: {
    type: DataTypes.STRING(45),
    allowNull: false
  },
  currency: {
    type: DataTypes.STRING(3),
    allowNull: false,
    defaultValue: 'THB'
  },
  timezone: {
    type: DataTypes.STRING(45),
    allowNull: false,
    defaultValue: 'Asia/Bangkok'
  },
  dateFormat: {
    type: DataTypes.STRING(45),
    allowNull: false,
    defaultValue: 'YYYY-MM-DD HH:mm:ss'
  },
  status: {
    type: DataTypes.STRING(15),
    allowNull: false
  }
})

module.exports = partnerModel
