const { DataTypes } = require('sequelize')

const sequelize = require('../helpers/sequelize')

const masterBankModel = require('./masterBankModel')
const masterTransactionSubTypeModel = require('./masterTransactionSubTypeModel')
const masterTransactionTypeModel = require('./masterTransactionTypeModel')
const partnerModel = require('./partnerModel')

const transactionModel = sequelize.define('WP_TRANSACTION', {
  id: {
    type: DataTypes.INTEGER(11),
    primaryKey: true,
    allowNull: false,
    autoIncrement: true
  },
  partnerId: {
    type: DataTypes.INTEGER(11),
    allowNull: false
  },
  transactionTypeId: {
    type: DataTypes.INTEGER(11),
    allowNull: false
  },
  transactionSubTypeId: {
    type: DataTypes.INTEGER(11),
    allowNull: false
  },
  referenceNo: {
    type: DataTypes.STRING(45),
    allowNull: false
  },
  orderId: {
    type: DataTypes.STRING(45),
    allowNull: false
  },
  payerId: {
    type: DataTypes.INTEGER(11),
    allowNull: false
  },
  payerUid: {
    type: DataTypes.STRING(21),
    allowNull: false
  },
  payerName: {
    type: DataTypes.STRING(255),
    allowNull: false
  },
  payerWalletId: {
    type: DataTypes.STRING(15),
    allowNull: false
  },
  payeeId: {
    type: DataTypes.INTEGER(11),
    allowNull: false
  },
  payeeUid: {
    type: DataTypes.STRING(21),
    allowNull: false
  },
  payeeName: {
    type: DataTypes.STRING(255),
    allowNull: false
  },
  payeeWalletId: {
    type: DataTypes.STRING(15),
    allowNull: false
  },
  bankId: {
    type: DataTypes.INTEGER(11)
  },
  bankBranch: {
    type: DataTypes.STRING(255)
  },
  bankAccountNumber: {
    type: DataTypes.STRING(15)
  },
  bankAccountName: {
    type: DataTypes.STRING(100)
  },
  feeExudeId: {
    type: DataTypes.INTEGER(11)
  },
  feeExudeName: {
    type: DataTypes.STRING(45)
  },
  feeExudeWalletId: {
    type: DataTypes.STRING(15)
  },
  discountAbsorbId: {
    type: DataTypes.INTEGER(11)
  },
  discountAbsorbName: {
    type: DataTypes.STRING(45)
  },
  discountAbsorbWalletId: {
    type: DataTypes.STRING(15)
  },
  discountCode: {
    type: DataTypes.STRING(45)
  },
  net: {
    type: DataTypes.DECIMAL(19, 4),
    allowNull: false,
    defaultValue: 0
  },
  fee: {
    type: DataTypes.DECIMAL(19, 4),
    allowNull: false,
    defaultValue: 0
  },
  discount: {
    type: DataTypes.DECIMAL(19, 4),
    allowNull: false,
    defaultValue: 0
  },
  total: {
    type: DataTypes.DECIMAL(19, 4),
    allowNull: false,
    defaultValue: 0
  },
  currency: {
    type: DataTypes.STRING(3),
    allowNull: false,
    defaultValue: 'THB'
  },
  approvalCode: {
    type: DataTypes.STRING(6)
  },
  failedReason: {
    type: DataTypes.STRING(255)
  },
  status: {
    type: DataTypes.STRING(15),
    allowNull: false
  },
  callbackUrl: {
    type: DataTypes.STRING(255)
  },
  notifyUrl: {
    type: DataTypes.STRING(255)
  },
  batchNo: {
    type: DataTypes.STRING(12)
  },
  reference1: {
    type: DataTypes.STRING(45)
  },
  reference2: {
    type: DataTypes.STRING(45)
  },
  reference3: {
    type: DataTypes.STRING(45)
  },
  externalReference: {
    type: DataTypes.STRING(45)
  },
  remark: {
    type: DataTypes.STRING(255)
  },
  voidedById: {
    type: DataTypes.INTEGER(11)
  },
  voidedByName: {
    type: DataTypes.STRING(255)
  },
  settledById: {
    type: DataTypes.INTEGER(11)
  },
  settledByName: {
    type: DataTypes.STRING(255)
  },
  expiryAt: {
    type: DataTypes.DATE()
  },
  holdedAt: {
    type: DataTypes.DATE()
  },
  approvedAt: {
    type: DataTypes.DATE()
  },
  cancelledAt: {
    type: DataTypes.DATE()
  },
  failedAt: {
    type: DataTypes.DATE()
  },
  voidedAt: {
    type: DataTypes.DATE()
  },
  refundedAt: {
    type: DataTypes.DATE()
  },
  settledAt: {
    type: DataTypes.DATE()
  }
})

transactionModel.hasOne(partnerModel, { as: 'partner', foreignKey: 'id', sourceKey: 'partnerId' })
transactionModel.hasOne(masterBankModel, { as: 'bank', foreignKey: 'id', sourceKey: 'bankId' })
transactionModel.hasOne(masterTransactionTypeModel, { as: 'type', foreignKey: 'id', sourceKey: 'transactionTypeId' })
transactionModel.hasOne(masterTransactionSubTypeModel, { as: 'subType', foreignKey: 'id', sourceKey: 'transactionSubTypeId' })

module.exports = transactionModel
