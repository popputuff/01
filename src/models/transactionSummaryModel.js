const { DataTypes } = require('sequelize')

const sequelize = require('../helpers/sequelize')

const transactionSummaryModel = sequelize.define('WP_TRANSACTION_SUMMARY', {
  id: {
    type: DataTypes.INTEGER(11),
    primaryKey: true,
    allowNull: false,
    autoIncrement: true
  },
  partnerId: {
    type: DataTypes.INTEGER(11),
    allowNull: false
  },
  transactionTypeId: {
    type: DataTypes.INTEGER(11),
    allowNull: false
  },
  transactionSubTypeId: {
    type: DataTypes.INTEGER(11),
    allowNull: false,
    defaultValue: 0
  },
  daily: {
    type: DataTypes.DECIMAL(19, 4),
    allowNull: false,
    defaultValue: 0
  },
  weekly: {
    type: DataTypes.DECIMAL(19, 4),
    allowNull: false,
    defaultValue: 0
  },
  monthly: {
    type: DataTypes.DECIMAL(19, 4),
    allowNull: false,
    defaultValue: 0
  },
  yearly: {
    type: DataTypes.DECIMAL(19, 4),
    allowNull: false,
    defaultValue: 0
  }
})

module.exports = transactionSummaryModel
