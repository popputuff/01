const { DataTypes } = require('sequelize')

const sequelize = require('../helpers/sequelize')

const masterTransactionTypeModel = require('./masterTransactionTypeModel')
const partnerModel = require('./partnerModel')

const transactionSettlementBatchModel = sequelize.define('WP_TRANSACTION_SETTLEMENT_BATCH', {
  id: {
    type: DataTypes.INTEGER(11),
    primaryKey: true,
    allowNull: false,
    autoIncrement: true
  },
  partnerId: {
    type: DataTypes.INTEGER(11),
    allowNull: false
  },
  transactionTypeId: {
    type: DataTypes.INTEGER(11),
    allowNull: false
  },
  batchDate: {
    type: DataTypes.DATEONLY(),
    allowNull: false
  },
  batchNo: {
    type: DataTypes.STRING(12),
    allowNull: false
  },
  key: {
    type: DataTypes.STRING(100),
    allowNull: false
  },
  status: {
    type: DataTypes.STRING(15),
    allowNull: false
  }
})

transactionSettlementBatchModel.hasOne(masterTransactionTypeModel, { as: 'transactionType', foreignKey: 'id', sourceKey: 'transactionTypeId' })
transactionSettlementBatchModel.hasOne(partnerModel, { as: 'partner', foreignKey: 'id', sourceKey: 'partnerId' })

module.exports = transactionSettlementBatchModel
