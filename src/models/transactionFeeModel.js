const { DataTypes } = require('sequelize')

const sequelize = require('../helpers/sequelize')

const transactionFeeModel = sequelize.define('WP_TRANSACTION_FEE', {
  id: {
    type: DataTypes.INTEGER(11),
    primaryKey: true,
    allowNull: false,
    autoIncrement: true
  },
  partnerId: {
    type: DataTypes.INTEGER(11),
    allowNull: false
  },
  transactionTypeId: {
    type: DataTypes.INTEGER(11),
    allowNull: false
  },
  transactionSubTypeId: {
    type: DataTypes.INTEGER(11),
    allowNull: false
  },
  start: {
    type: DataTypes.DECIMAL(19, 4),
    allowNull: false,
    defaultValue: 0
  },
  end: {
    type: DataTypes.DECIMAL(19, 4),
    allowNull: false,
    defaultValue: 0
  },
  fee: {
    type: DataTypes.DECIMAL(19, 4),
    allowNull: false,
    defaultValue: 0
  }
})

module.exports = transactionFeeModel
