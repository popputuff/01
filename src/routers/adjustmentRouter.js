const express = require('express')
const { validate } = require('express-validation')

const transactionSubTypeMaster = require('../constants/masters/transactionSubTypeMaster')
const transactionTypeMaster = require('../constants/masters/transactionTypeMaster')
const userTypeMaster = require('../constants/masters/userTypeMaster')

const sequelizeTransactionController = require('../controllers/sequelizeTransactionController')
const transactionConfigController = require('../controllers/transactionConfigController')
const transactionController = require('../controllers/transactionController')
const userController = require('../controllers/userController')

const adjustmentValidation = require('../validations/adjustmentValidation')

const router = express.Router()

router.post('/inquiry',
  validate(adjustmentValidation.inquiryAdjustment),
  userController.findUser('payer', 'body', null, null, 'payer'),
  userController.findUser('payee', 'body', null, null, 'payee'),
  sequelizeTransactionController.createSqlTransaction(),
  transactionController.createTransaction(
    transactionTypeMaster.ADJUSTMENT,
    transactionSubTypeMaster.CLOSELOOP
  ),
  sequelizeTransactionController.commitSqlTransaction(),
  sequelizeTransactionController.handleSqlTransactionFailed(),
  (req, res, next) => {
    let balanceBefore = 0
    let balanceAfter = 0
    if (req.payer.userTypeId === userTypeMaster.CONSUMER.id) {
      balanceBefore = Number(req.payer.wallets[0].balance)
      balanceAfter = Number(req.payer.wallets[0].balance) - Number(req.transaction.total)
    } else if (req.payee.userTypeId === userTypeMaster.CONSUMER.id) {
      balanceBefore = Number(req.payee.wallets[0].balance)
      balanceAfter = Number(req.payee.wallets[0].balance) + Number(req.transaction.total)
    }

    req.response = {
      res_code: '0000',
      res_desc: '',
      balance_before: balanceBefore,
      balance_after: balanceAfter,
      order_id: req.transaction.orderId,
      reference_no: req.transaction.referenceNo,
      payer_name: req.transaction.payerName,
      payee_name: req.transaction.payeeName,
      total: req.transaction.total,
      currency: req.transaction.currency,
      remark: req.transaction.remark
    }
    res.json(req.response)
    next()
  }
)

router.post('/confirm',
  validate(adjustmentValidation.confirmAdjustment),
  sequelizeTransactionController.createSqlTransaction(),
  transactionController.findTransaction('referenceNo', 'body'),
  transactionConfigController.findTransactionConfig(),
  transactionController.approveTransaction(),
  sequelizeTransactionController.commitSqlTransaction(),
  sequelizeTransactionController.handleSqlTransactionFailed(),
  (req, res, next) => {
    req.response = {
      res_code: '0000',
      res_desc: '',
      order_id: req.transaction.orderId,
      reference_no: req.transaction.referenceNo,
      payer_name: req.transaction.payerName,
      payee_name: req.transaction.payeeName,
      total: Number(req.transaction.total),
      currency: req.transaction.currency,
      approval_code: req.transaction.approvalCode,
      remark: req.transaction.remark
    }
    res.json(req.response)
    next()
  }
)

module.exports = router
