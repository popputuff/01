const express = require('express')
const { validate } = require('express-validation')
const dayjs = require('dayjs')

dayjs.extend(require('dayjs/plugin/utc'))
dayjs.extend(require('dayjs/plugin/timezone'))

const octopusServiceController = require('../controllers/octopusServiceController')
const sequelizeTransactionController = require('../controllers/sequelizeTransactionController')
const transactionController = require('../controllers/transactionController')
const transactionSummaryController = require('../controllers/transactionSummaryController')

const transactionValidation = require('../validations/transactionValidation')

const router = express.Router()

router.get('/',
  validate(transactionValidation.findTransactions),
  transactionController.findTransactions(),
  (req, res, next) => {
    const timezone = req.headers['x-timezone']
    const dateFormat = req.headers['x-date-format']

    req.response = {
      res_code: '0000',
      res_desc: '',
      total: req.total,
      transactions: req.transactions.map((transaction) => ({
        id: transaction.id,
        partner_id: transaction.partner.id,
        partner_name: transaction.partner.name,
        reference_no: transaction.referenceNo,
        order_id: transaction.orderId,
        type: transaction.type.name,
        sub_type: transaction.subType.name,
        payer_uid: transaction.payerUid,
        payer_name: transaction.payerName,
        payer_wallet_id: transaction.payerWalletId,
        payee_uid: transaction.payeeUid,
        payee_name: transaction.payeeName,
        payee_wallet_id: transaction.payeeWalletId,
        bank: transaction.bank ? transaction.bank.shortName : undefined,
        bank_account_number: transaction.bankAccountNumber ? transaction.bankAccountNumber.replace(/(\d{5})(.*)(\d)/, '*****$2*') : undefined,
        fee_exude_name: transaction.feeExudeName,
        fee_exude_wallet_id: transaction.feeExudeWalletId,
        discount_absorb_name: transaction.discountAbsorbName,
        discount_absorb_wallet_id: transaction.discountAbsorbWalletId,
        net: Number(transaction.net),
        fee: Number(transaction.fee),
        discount: Number(transaction.discount),
        total: Number(transaction.total),
        currency: transaction.currency,
        discount_code: transaction.discountCode,
        approval_code: transaction.approvalCode,
        status: transaction.status,
        reference1: transaction.reference1,
        reference2: transaction.reference2,
        reference3: transaction.reference3,
        external_reference: transaction.externalReference,
        created_at: dayjs.utc(transaction.createdAt).tz(timezone).format(dateFormat),
        holded_at: transaction.holdedAt ? dayjs.utc(transaction.holdedAt).tz(timezone).format(dateFormat) : null,
        approved_at: transaction.approvedAt ? dayjs.utc(transaction.approvedAt).tz(timezone).format(dateFormat) : null,
        cancelled_at: transaction.cancelledAt ? dayjs.utc(transaction.cancelledAt).tz(timezone).format(dateFormat) : null,
        failed_at: transaction.failedAt ? dayjs.utc(transaction.failedAt).tz(timezone).format(dateFormat) : null,
        voided_at: transaction.voidedAt ? dayjs.utc(transaction.voidedAt).tz(timezone).format(dateFormat) : null,
        refunded_at: transaction.refundedAt ? dayjs.utc(transaction.refundedAt).tz(timezone).format(dateFormat) : null,
        settled_at: transaction.settledAt ? dayjs.utc(transaction.settledAt).tz(timezone).format(dateFormat) : null,
        updated_at: dayjs.utc(transaction.updatedAt).tz(timezone).format(dateFormat)
      }))
    }
    res.json(req.response)
    next()
  }
)

router.get('/:id',
  validate(transactionValidation.findTransaction),
  transactionController.findTransaction('id', 'param'),
  (req, res, next) => {
    const timezone = req.headers['x-timezone']
    const dateFormat = req.headers['x-date-format']

    req.response = {
      res_code: '0000',
      res_desc: '',
      id: req.transaction.id,
      partner_id: req.transaction.partner.id,
      partner_name: req.transaction.partner.name,
      reference_no: req.transaction.referenceNo,
      order_id: req.transaction.orderId,
      type: req.transaction.type.name,
      sub_type: req.transaction.subType.name,
      payer_uid: req.transaction.payerUid,
      payer_name: req.transaction.payerName,
      payer_wallet_id: req.transaction.payerWalletId,
      payee_uid: req.transaction.payeeUid,
      payee_name: req.transaction.payeeName,
      payee_wallet_id: req.transaction.payeeWalletId,
      bank: req.transaction.bank ? req.transaction.bank.shortName : undefined,
      bank_branch: req.transaction.bankBracnh,
      bank_account_number: req.transaction.bankAccountNumber ? req.transaction.bankAccountNumber.replace(/(\d{5})(.*)(\d)/, '*****$2*') : undefined,
      bank_account_name: req.transaction.bankAccountName,
      fee_exude_name: req.transaction.feeExudeName,
      fee_exude_wallet_id: req.transaction.feeExudeWalletId,
      discount_absorb_name: req.transaction.discountAbsorbName,
      discount_absorb_wallet_id: req.transaction.discountAbsorbWalletId,
      net: Number(req.transaction.net),
      fee: Number(req.transaction.fee),
      discount: Number(req.transaction.discount),
      total: Number(req.transaction.total),
      currency: req.transaction.currency,
      discount_code: req.transaction.discountCode,
      approval_code: req.transaction.approvalCode,
      status: req.transaction.status,
      callback_url: req.transaction.callbackUrl,
      notify_url: req.transaction.notifyUrl,
      batch_no: req.transaction.batchNo,
      reference1: req.transaction.reference1,
      reference2: req.transaction.reference2,
      reference3: req.transaction.reference3,
      external_reference: req.transaction.externalReference,
      remark: req.transaction.remark,
      voided_by_id: req.transaction.voidedById,
      voided_by_name: req.transaction.voidedByName,
      settled_by_id: req.transaction.settledById,
      settled_by_name: req.transaction.settledByName,
      created_at: dayjs.utc(req.transaction.createdAt).tz(timezone).format(dateFormat),
      holded_at: req.transaction.holdedAt ? dayjs.utc(req.transaction.holdedAt).tz(timezone).format(dateFormat) : null,
      approved_at: req.transaction.approvedAt ? dayjs.utc(req.transaction.approvedAt).tz(timezone).format(dateFormat) : null,
      cancelled_at: req.transaction.cancelledAt ? dayjs.utc(req.transaction.calcelledAt).tz(timezone).format(dateFormat) : null,
      failed_at: req.transaction.failedAt ? dayjs.utc(req.transaction.failedAt).tz(timezone).format(dateFormat) : null,
      voided_at: req.transaction.voidedAt ? dayjs.utc(req.transaction.voidedAt).tz(timezone).format(dateFormat) : null,
      refunded_at: req.transaction.refundedAt ? dayjs.utc(req.transaction.refundedAt).tz(timezone).format(dateFormat) : null,
      settled_at: req.transaction.settledAt ? dayjs.utc(req.transaction.settledAt).tz(timezone).format(dateFormat) : null,
      updated_at: dayjs.utc(req.transaction.updatedAt).tz(timezone).format(dateFormat)
    }
    res.json(req.response)
    next()
  }
)

router.get('/:id/inquiry',
  validate(transactionValidation.inquiryTransaction),
  sequelizeTransactionController.createSqlTransaction(),
  transactionController.findTransaction('id', 'param'),
  octopusServiceController.findOctopusCredential('partnerId', 'transaction'),
  octopusServiceController.findOctopusPaymentProfile('partnerId', 'transaction'),
  octopusServiceController.inquiryTransaction(),
  sequelizeTransactionController.commitSqlTransaction(),
  sequelizeTransactionController.handleSqlTransactionFailed(),
  (req, res, next) => {
    req.response = {
      res_code: '0000',
      res_desc: ''
    }
    res.json(req.response)
    next()
  }
)

router.patch('/:id/approve',
  validate(transactionValidation.approveTransaction),
  sequelizeTransactionController.createSqlTransaction(),
  transactionController.findTransaction('id', 'param'),
  transactionController.approveHoldedTransaction(),
  sequelizeTransactionController.commitSqlTransaction(),
  sequelizeTransactionController.handleSqlTransactionFailed(),
  (req, res, next) => {
    const timezone = req.headers['x-timezone']
    const dateFormat = req.headers['x-date-format']

    req.response = {
      res_code: '0000',
      res_desc: '',
      approval_code: req.transaction.approvalCode,
      approved_at: dayjs.utc(req.transaction.approvedAt).tz(timezone).format(dateFormat)
    }
    res.json(req.response)
    next()
  }
)

router.patch('/:id/void',
  validate(transactionValidation.voidTransaction),
  sequelizeTransactionController.createSqlTransaction(),
  transactionController.findTransaction('id', 'param'),
  octopusServiceController.findOctopusCredential('partnerId', 'transaction'),
  octopusServiceController.findOctopusPaymentProfile('partnerId', 'transaction'),
  transactionController.voidTransaction(),
  octopusServiceController.voidTransaction(),
  transactionSummaryController.decrementTransactionSummary(),
  sequelizeTransactionController.commitSqlTransaction(),
  sequelizeTransactionController.handleSqlTransactionFailed(),
  (req, res, next) => {
    req.response = {
      res_code: '0000',
      res_desc: ''
    }
    res.json(req.response)
    next()
  }
)

module.exports = router
