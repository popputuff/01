const express = require('express')
const { validate } = require('express-validation')

const transactionSubTypeMaster = require('../constants/masters/transactionSubTypeMaster')
const transactionTypeMaster = require('../constants/masters/transactionTypeMaster')
const userTypeMaster = require('../constants/masters/userTypeMaster')
const walletTypeMaster = require('../constants/masters/walletTypeMaster')

const sequelizeTransactionController = require('../controllers/sequelizeTransactionController')
const transactionConfigController = require('../controllers/transactionConfigController')
const transactionController = require('../controllers/transactionController')
const transactionFeeController = require('../controllers/transactionFeeController')
const transactionSummaryController = require('../controllers/transactionSummaryController')
const userController = require('../controllers/userController')

const transferValidation = require('../validations/transferValidation')

const router = express.Router()

router.post('/inquiry',
  validate(transferValidation.inquiryTransfer),
  userController.findUser(null, null, userTypeMaster.PARTNER, null, 'partner'),
  userController.findUser('payer', 'body', null, null, 'payer'),
  userController.findUser('payee', 'body', null, null, 'payee'),
  sequelizeTransactionController.createSqlTransaction(),
  transactionFeeController.findFee(),
  transactionController.validatePayerBalance(walletTypeMaster.CUSTOMER),
  transactionController.validateLimitPerTransaction(
    transactionTypeMaster.TRANSFER,
    transactionSubTypeMaster.CLOSELOOP
  ),
  transactionController.findWalletSummaryPerDay(
    walletTypeMaster.CUSTOMER,
    'payer'
  ),
  transactionController.validateLimitPerDay(
    transactionTypeMaster.TRANSFER,
    transactionSubTypeMaster.CLOSELOOP,
    'payer'
  ),
  transactionController.createTransaction(
    transactionTypeMaster.TRANSFER,
    transactionSubTypeMaster.CLOSELOOP,
    walletTypeMaster.CUSTOMER,
    walletTypeMaster.CUSTOMER
  ),
  sequelizeTransactionController.commitSqlTransaction(),
  sequelizeTransactionController.handleSqlTransactionFailed(),
  (req, res, next) => {
    req.response = {
      res_code: '0000',
      res_desc: '',
      order_id: req.transaction.orderId,
      reference_no: req.transaction.referenceNo,
      payer_name: req.transaction.payerName,
      payee_name: req.transaction.payeeName,
      net: req.transaction.net,
      fee: req.transaction.fee,
      total: req.transaction.total,
      currency: req.transaction.currency
    }
    res.json(req.response)
    next()
  }
)

router.post('/confirm',
  validate(transferValidation.confirmTransfer),
  sequelizeTransactionController.createSqlTransaction(),
  transactionController.findTransaction('referenceNo', 'body'),
  transactionController.findWalletSummaryPerDay(
    walletTypeMaster.CUSTOMER,
    'payer'
  ),
  transactionController.validateLimitPerDay(
    transactionTypeMaster.TRANSFER,
    transactionSubTypeMaster.CLOSELOOP,
    'payer'
  ),
  transactionConfigController.findTransactionConfig(),
  transactionController.approveTransaction(),
  transactionSummaryController.incrementTransactionSummary(),
  sequelizeTransactionController.commitSqlTransaction(),
  sequelizeTransactionController.handleSqlTransactionFailed(),
  (req, res, next) => {
    req.response = {
      res_code: '0000',
      res_desc: '',
      order_id: req.transaction.orderId,
      reference_no: req.transaction.referenceNo,
      payer_name: req.transaction.payerName,
      payee_name: req.transaction.payeeName,
      net: Number(req.transaction.net),
      fee: Number(req.transaction.fee),
      total: Number(req.transaction.total),
      currency: req.transaction.currency,
      approval_code: req.transaction.approvalCode
    }
    res.json(req.response)
    next()
  }
)

module.exports = router
