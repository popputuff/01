const express = require('express')
const { validate } = require('express-validation')
const dayjs = require('dayjs')

dayjs.extend(require('dayjs/plugin/utc'))
dayjs.extend(require('dayjs/plugin/timezone'))

const transactionController = require('../controllers/transactionController')

const historyValidation = require('../validations/historyValidation')

const router = express.Router()

router.get('/',
  validate(historyValidation.history),
  transactionController.findTransactions({ isExcludePending: true }),
  (req, res, next) => {
    const timezone = req.headers['x-timezone']
    const dateFormat = req.headers['x-date-format']

    req.response = {
      res_code: '0000',
      res_desc: '',
      total: req.total,
      transactions: req.transactions.map((transaction) => ({
        id: transaction.id,
        reference_no: transaction.referenceNo,
        order_id: transaction.orderId,
        type: transaction.type.name,
        sub_type: transaction.subType.name,
        payer_uid: transaction.payerUid,
        payer_name: transaction.payerName,
        payer_wallet_id: transaction.payerWalletId,
        payee_uid: transaction.payeeUid,
        payee_name: transaction.payeeName,
        payee_wallet_id: transaction.payeeWalletId,
        bank: transaction.bank ? transaction.bank.shortName : undefined,
        bank_account_number: transaction.bankAccountNumber ? transaction.bankAccountNumber.replace(/(\d{5})(.*)(\d)/, '*****$2*') : undefined,
        net: Number(transaction.net),
        fee: Number(transaction.fee),
        discount: Number(transaction.discount),
        total: Number(transaction.total),
        currency: transaction.currency,
        discount_code: transaction.discountCode,
        approval_code: transaction.approvalCode,
        status: transaction.status,
        reference1: transaction.reference1,
        reference2: transaction.reference2,
        reference3: transaction.reference3,
        created_at: dayjs.utc(transaction.createdAt).tz(timezone).format(dateFormat),
        holded_at: transaction.holdedAt ? dayjs.utc(transaction.holdedAt).tz(timezone).format(dateFormat) : null,
        approved_at: transaction.approvedAt ? dayjs.utc(transaction.approvedAt).tz(timezone).format(dateFormat) : null,
        cancelled_at: transaction.cancelledAt ? dayjs.utc(transaction.cancelledAt).tz(timezone).format(dateFormat) : null,
        failed_at: transaction.failedAt ? dayjs.utc(transaction.failedAt).tz(timezone).format(dateFormat) : null,
        voided_at: transaction.voidedAt ? dayjs.utc(transaction.voidedAt).tz(timezone).format(dateFormat) : null,
        refunded_at: transaction.refundedAt ? dayjs.utc(transaction.refundedAt).tz(timezone).format(dateFormat) : null,
        settled_at: transaction.settledAt ? dayjs.utc(transaction.settledAt).tz(timezone).format(dateFormat) : null,
        updated_at: dayjs.utc(transaction.updatedAt).tz(timezone).format(dateFormat)
      }))
    }
    res.json(req.response)
    next()
  }
)

module.exports = router
