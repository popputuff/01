const express = require('express')
const { validate } = require('express-validation')

const sequelizeTransactionController = require('../controllers/sequelizeTransactionController')
const notificationController = require('../controllers/notificationController')
const octopusServiceController = require('../controllers/octopusServiceController')
const transactionConfigController = require('../controllers/transactionConfigController')
const transactionController = require('../controllers/transactionController')
const transactionSummaryController = require('../controllers/transactionSummaryController')

const notifyValidation = require('../validations/notifyValidation')

const router = express.Router()

router.post('/',
  validate(notifyValidation.notify),
  octopusServiceController.findOctopusCredential('partnerId', 'header'),
  octopusServiceController.validateSignData(),
  sequelizeTransactionController.createSqlTransaction(),
  transactionController.findTransaction('orderId', 'body.data'),
  octopusServiceController.executeNotifyTransaction(),
  transactionConfigController.findTransactionConfig(),
  transactionController.approveTransaction(),
  transactionSummaryController.incrementTransactionSummary(),
  sequelizeTransactionController.commitSqlTransaction(),
  sequelizeTransactionController.handleSqlTransactionFailed(),
  notificationController.sendNotification(),
  (req, res, next) => {
    req.response = {
      res_code: '0000',
      res_desc: ''
    }
    res.json(req.response)
    next()
  }
)

module.exports = router
