const express = require('express')
const dayjs = require('dayjs')
const { validate } = require('express-validation')

const transactionSettlementBatchController = require('../controllers/transactionSettlementBatchController')

const settlementValidation = require('../validations/settlementValidation')

const router = express.Router()

router.get('/batch',
  validate(settlementValidation.findTransactionSettlementBatches),
  transactionSettlementBatchController.findTransactionSettlementBatches(),
  (req, res, next) => {
    const timezone = req.headers['x-timezone']
    const dateFormat = req.headers['x-date-format']
    req.response = {
      res_code: '0000',
      res_desc: '',
      total: req.total,
      batches: req.batches.map((batch) => ({
        id: batch.id,
        partner_id: batch.partner.id,
        partner_name: batch.partner.name,
        transaction_type: batch.transactionType.name,
        batch_no: batch.batchNo,
        batch_date: batch.batchDate,
        status: batch.status,
        created_at: dayjs.utc(batch.createdAt).tz(timezone).format(dateFormat),
        updated_at: dayjs.utc(batch.updatedAt).tz(timezone).format(dateFormat)
      }))
    }
    res.json(req.response)
    next()
  }
)

router.get('/batch/:id',
  validate(settlementValidation.findTransactionSettlementBatch),
  transactionSettlementBatchController.findTransactionSettlementBatch(),
  transactionSettlementBatchController.processTransactionSettlementBatch(),
  (req, res, next) => {
    req.response = {
      res_code: '0000',
      res_desc: '',
      batch: req.batch
    }
    res.json(req.response)
    next()
  }
)

router.patch('/batch/:id/settle',
  validate(settlementValidation.settleTransactionSettlementBatch),
  transactionSettlementBatchController.findTransactionSettlementBatch(),
  transactionSettlementBatchController.settleTransactionSettlementBatch(),
  (req, res, next) => {
    req.response = {
      res_code: '0000',
      res_desc: '',
      batch: req.batch
    }
    res.json(req.response)
    next()
  }
)

module.exports = router
