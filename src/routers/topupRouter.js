const express = require('express')
const { validate } = require('express-validation')

const transactionSubTypeMaster = require('../constants/masters/transactionSubTypeMaster')
const transactionTypeMaster = require('../constants/masters/transactionTypeMaster')
const userTypeMaster = require('../constants/masters/userTypeMaster')
const walletTypeMaster = require('../constants/masters/walletTypeMaster')

const octopusServiceController = require('../controllers/octopusServiceController')
const sequelizeTransactionController = require('../controllers/sequelizeTransactionController')
const transactionController = require('../controllers/transactionController')
const transactionFeeController = require('../controllers/transactionFeeController')
const transactionSummaryController = require('../controllers/transactionSummaryController')
const userController = require('../controllers/userController')

const topupValidation = require('../validations/topupValidation')

const router = express.Router()

router.post('/promptpay',
  validate(topupValidation.topupPromptpay),
  userController.findUser(null, null, userTypeMaster.SYSTEM, null, 'system'),
  userController.findUser(null, null, userTypeMaster.PARTNER, null, 'payer'),
  userController.findUser('walletId', 'body', null, null, 'payee'),
  sequelizeTransactionController.createSqlTransaction(),
  transactionController.validateLimitPerTransaction(
    transactionTypeMaster.TOPUP,
    transactionSubTypeMaster.PROMPTPAY
  ),
  transactionController.findWalletSummaryPerDay(walletTypeMaster.CUSTOMER, 'payee'),
  transactionController.validateLimitPerDay(
    transactionTypeMaster.TOPUP,
    transactionSubTypeMaster.PROMPTPAY,
    'payee'
  ),
  transactionFeeController.findFee(
    transactionTypeMaster.TOPUP,
    transactionSubTypeMaster.PROMPTPAY
  ),
  transactionController.createTransaction(
    transactionTypeMaster.TOPUP,
    transactionSubTypeMaster.PROMPTPAY,
    walletTypeMaster.COLLATERAL,
    walletTypeMaster.CUSTOMER
  ),
  octopusServiceController.findOctopusCredential('partnerId', 'header'),
  octopusServiceController.findOctopusPaymentProfile('partnerId', 'header'),
  octopusServiceController.createPayment('QR_PP_TAG30'),
  sequelizeTransactionController.commitSqlTransaction(),
  sequelizeTransactionController.handleSqlTransactionFailed(),
  (req, res, next) => {
    req.response = {
      res_code: '0000',
      res_desc: '',
      order_id: req.transaction.orderId,
      reference_no: req.transaction.referenceNo,
      redirect_url: req.redirectUrl
    }
    res.json(req.response)
    next()
  }
)

router.post('/credit-card',
  validate(topupValidation.topupCreditCard),
  userController.findUser(null, null, userTypeMaster.SYSTEM, null, 'system'),
  userController.findUser(null, null, userTypeMaster.PARTNER, null, 'payer'),
  userController.findUser('walletId', 'body', null, null, 'payee'),
  sequelizeTransactionController.createSqlTransaction(),
  transactionController.validateLimitPerTransaction(
    transactionTypeMaster.TOPUP,
    transactionSubTypeMaster.CREDITCARD
  ),
  transactionController.findWalletSummaryPerDay(walletTypeMaster.CUSTOMER, 'payee'),
  transactionController.validateLimitPerDay(
    transactionTypeMaster.TOPUP,
    transactionSubTypeMaster.CREDITCARD,
    'payee'
  ),
  transactionFeeController.findFee(
    transactionTypeMaster.TOPUP,
    transactionSubTypeMaster.CREDITCARD
  ),
  transactionController.createTransaction(
    transactionTypeMaster.TOPUP,
    transactionSubTypeMaster.CREDITCARD,
    walletTypeMaster.COLLATERAL,
    walletTypeMaster.CUSTOMER
  ),
  octopusServiceController.findOctopusCredential('partnerId', 'header'),
  octopusServiceController.findOctopusPaymentProfile('partnerId', 'header'),
  octopusServiceController.createPayment('FULL'),
  sequelizeTransactionController.commitSqlTransaction(),
  sequelizeTransactionController.handleSqlTransactionFailed(),
  (req, res, next) => {
    req.response = {
      res_code: '0000',
      res_desc: '',
      order_id: req.transaction.orderId,
      reference_no: req.transaction.referenceNo,
      redirect_url: req.redirectUrl
    }
    res.json(req.response)
    next()
  }
)

router.post('/direct',
  validate(topupValidation.topupDirect),
  userController.findUser(null, null, userTypeMaster.SYSTEM, null, 'system'),
  userController.findUser(null, null, userTypeMaster.PARTNER, null, 'payer'),
  userController.findUser('walletId', 'body', null, null, 'payee'),
  sequelizeTransactionController.createSqlTransaction(),
  transactionController.validateLimitPerTransaction(
    transactionTypeMaster.TOPUP,
    transactionSubTypeMaster.DIRECT
  ),
  transactionController.findWalletSummaryPerDay(walletTypeMaster.CUSTOMER, 'payee'),
  transactionController.validateLimitPerDay(
    transactionTypeMaster.TOPUP,
    transactionSubTypeMaster.DIRECT,
    'payee'
  ),
  transactionFeeController.findFee(
    transactionTypeMaster.TOPUP,
    transactionSubTypeMaster.DIRECT
  ),
  transactionController.createTransaction(
    transactionTypeMaster.TOPUP,
    transactionSubTypeMaster.DIRECT,
    walletTypeMaster.COLLATERAL,
    walletTypeMaster.CUSTOMER
  ),
  transactionController.approveTransaction(),
  transactionSummaryController.incrementTransactionSummary(),
  sequelizeTransactionController.commitSqlTransaction(),
  sequelizeTransactionController.handleSqlTransactionFailed(),
  (req, res, next) => {
    req.response = {
      res_code: '0000',
      res_desc: '',
      order_id: req.transaction.orderId,
      reference_no: req.transaction.referenceNo
    }
    res.json(req.response)
    next()
  }
)

module.exports = router
