const express = require('express')
const { validate } = require('express-validation')

const transactionSubTypeMaster = require('../constants/masters/transactionSubTypeMaster')
const transactionTypeMaster = require('../constants/masters/transactionTypeMaster')
const userTypeMaster = require('../constants/masters/userTypeMaster')
const walletTypeMaster = require('../constants/masters/walletTypeMaster')

const sequelizeTransactionController = require('../controllers/sequelizeTransactionController')
const transactionConfigController = require('../controllers/transactionConfigController')
const transactionController = require('../controllers/transactionController')
const transactionFeeController = require('../controllers/transactionFeeController')
const transactionSummaryController = require('../controllers/transactionSummaryController')
const userController = require('../controllers/userController')

const paymentValidation = require('../validations/paymentValidation')

const router = express.Router()

router.post('/inquiry',
  validate(paymentValidation.inquiryPayment),
  userController.findUser(null, null, userTypeMaster.PARTNER, null, 'partner'),
  userController.findUser('payer', 'body', null, null, 'payer'),
  userController.findUser('payee', 'body', null, null, 'payee'),
  sequelizeTransactionController.createSqlTransaction(),
  transactionFeeController.findFee(
    transactionTypeMaster.PAYMENT,
    transactionSubTypeMaster.CLOSELOOP
  ),
  transactionController.validatePayerBalance(walletTypeMaster.CUSTOMER),
  transactionController.validateLimitPerTransaction(
    transactionTypeMaster.PAYMENT,
    transactionSubTypeMaster.CLOSELOOP
  ),
  transactionController.findWalletSummaryPerDay(walletTypeMaster.CUSTOMER, 'payer'),
  transactionController.validateLimitPerDay(
    transactionTypeMaster.PAYMENT,
    transactionSubTypeMaster.CLOSELOOP,
    'payer'
  ),
  transactionController.createTransaction(
    transactionTypeMaster.PAYMENT,
    transactionSubTypeMaster.CLOSELOOP,
    walletTypeMaster.CUSTOMER,
    walletTypeMaster.CUSTOMER
  ),
  sequelizeTransactionController.commitSqlTransaction(),
  sequelizeTransactionController.handleSqlTransactionFailed(),
  (req, res, next) => {
    req.response = {
      res_code: '0000',
      res_desc: '',
      order_id: req.transaction.orderId,
      reference_no: req.transaction.referenceNo,
      payer_name: req.transaction.payerName,
      payee_name: req.transaction.payeeName,
      net: req.transaction.net,
      fee: req.transaction.fee,
      discount: req.transaction.discount,
      total: req.transaction.total,
      currency: req.transaction.currency
    }
    res.json(req.response)
    next()
  }
)

router.post('/confirm',
  validate(paymentValidation.confirmPayment),
  sequelizeTransactionController.createSqlTransaction(),
  transactionController.findTransaction('referenceNo', 'body'),
  transactionController.findWalletSummaryPerDay(walletTypeMaster.CUSTOMER, 'payer'),
  transactionController.validateLimitPerDay(
    transactionTypeMaster.PAYMENT,
    transactionSubTypeMaster.CLOSELOOP,
    'payer'
  ),
  transactionConfigController.findTransactionConfig(),
  transactionController.approveTransaction(),
  transactionSummaryController.incrementTransactionSummary(),
  sequelizeTransactionController.commitSqlTransaction(),
  sequelizeTransactionController.handleSqlTransactionFailed(),
  (req, res, next) => {
    req.response = {
      res_code: '0000',
      res_desc: '',
      order_id: req.transaction.orderId,
      reference_no: req.transaction.referenceNo,
      payer_name: req.transaction.payerName,
      payee_name: req.transaction.payeeName,
      net: Number(req.transaction.net),
      fee: Number(req.transaction.fee),
      discount: Number(req.transaction.discount),
      total: Number(req.transaction.total),
      currency: req.transaction.currency
    }
    res.json(req.response)
    next()
  }
)

module.exports = router
