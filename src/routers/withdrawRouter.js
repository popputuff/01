const express = require('express')
const { validate } = require('express-validation')

const transactionSubTypeMaster = require('../constants/masters/transactionSubTypeMaster')
const transactionTypeMaster = require('../constants/masters/transactionTypeMaster')
const userTypeMaster = require('../constants/masters/userTypeMaster')
const walletTypeMaster = require('../constants/masters/walletTypeMaster')

const sequelizeTransactionController = require('../controllers/sequelizeTransactionController')
const transactionConfigController = require('../controllers/transactionConfigController')
const transactionController = require('../controllers/transactionController')
const transactionFeeController = require('../controllers/transactionFeeController')
const transactionSummaryController = require('../controllers/transactionSummaryController')
const userController = require('../controllers/userController')

const withdrawValidation = require('../validations/withdrawValidation')

const router = express.Router()

router.post('/casa/inquiry',
  validate(withdrawValidation.inquiryWithdrawCasa),
  userController.findUser(null, null, userTypeMaster.SYSTEM, null, 'system'),
  userController.findUser('walletId', 'body', userTypeMaster.CUSTOMER, null, 'payer'),
  userController.findUser(null, null, userTypeMaster.PARTNER, null, 'payee'),
  sequelizeTransactionController.createSqlTransaction(),
  transactionFeeController.findFee(
    transactionTypeMaster.WITHDRAW,
    transactionSubTypeMaster.CASA
  ),
  transactionController.validatePayerBalance(walletTypeMaster.CUSTOMER),
  transactionController.validateLimitPerTransaction(
    transactionTypeMaster.WITHDRAW,
    transactionSubTypeMaster.CASA
  ),
  transactionController.findWalletSummaryPerDay(walletTypeMaster.CUSTOMER, 'payer'),
  transactionController.validateLimitPerDay(
    transactionTypeMaster.WITHDRAW,
    transactionSubTypeMaster.CASA,
    'payer'
  ),
  transactionController.createTransaction(
    transactionTypeMaster.WITHDRAW,
    transactionSubTypeMaster.CASA,
    walletTypeMaster.CUSTOMER,
    walletTypeMaster.COLLATERAL
  ),
  sequelizeTransactionController.commitSqlTransaction(),
  sequelizeTransactionController.handleSqlTransactionFailed(),
  (req, res, next) => {
    req.response = {
      res_code: '0000',
      res_desc: '',
      order_id: req.transaction.orderId,
      reference_no: req.transaction.referenceNo,
      payer_name: req.transaction.payerName,
      payee_name: req.transaction.payeeName,
      net: req.transaction.net,
      fee: req.transaction.fee,
      discount: req.transaction.discount,
      total: req.transaction.total,
      currency: req.transaction.currency
    }
    res.json(req.response)
    next()
  }
)

router.post('/casa/confirm',
  validate(withdrawValidation.confirmWithdrawCasa),
  sequelizeTransactionController.createSqlTransaction(),
  transactionController.findTransaction('referenceNo', 'body'),
  transactionController.findWalletSummaryPerDay(walletTypeMaster.CUSTOMER, 'payer'),
  transactionController.validateLimitPerDay(
    transactionTypeMaster.WITHDRAW,
    transactionSubTypeMaster.CASA,
    'payer'
  ),
  transactionConfigController.findTransactionConfig(),
  transactionController.approveTransaction(),
  transactionSummaryController.incrementTransactionSummary(),
  sequelizeTransactionController.commitSqlTransaction(),
  sequelizeTransactionController.handleSqlTransactionFailed(),
  (req, res, next) => {
    req.response = {
      res_code: '0000',
      res_desc: '',
      order_id: req.transaction.orderId,
      reference_no: req.transaction.referenceNo,
      payer_name: req.transaction.payerName,
      payee_name: req.transaction.payeeName,
      net: Number(req.transaction.net),
      fee: Number(req.transaction.fee),
      discount: Number(req.transaction.discount),
      total: Number(req.transaction.total),
      currency: req.transaction.currency,
      approval_code: req.transaction.approvalCode
    }
    res.json(req.response)
    next()
  }
)

module.exports = router
