const { Joi } = require('express-validation')

const transactionStatusMaster = require('../constants/masters/transactionStatusMaster')
const transactionSubTypeMaster = require('../constants/masters/transactionSubTypeMaster')
const transactionTypeMaster = require('../constants/masters/transactionTypeMaster')

exports.findTransactions = {
  headers: Joi.object({
    'x-request-id': Joi.string().required(),
    'x-partner-id': Joi.string().allow('').optional(),
    'x-timezone': Joi.string().required(),
    'x-date-format': Joi.string().required(),
    'x-email': Joi.string().optional()
  }).unknown(true),
  query: Joi.object({
    is_export: Joi.boolean().optional(),
    q: Joi.string().allow('').optional(),
    type: Joi.string().allow('').valid(
      transactionTypeMaster.TOPUP.name,
      transactionTypeMaster.TRANSFER.name,
      transactionTypeMaster.PAYMENT.name,
      transactionTypeMaster.WITHDRAW.name,
      transactionTypeMaster.ADJUSTMENT.name
    ).optional(),
    sub_type: Joi.string().allow('').valid(
      transactionSubTypeMaster.CLOSELOOP.name,
      transactionSubTypeMaster.PROMPTPAY.name,
      transactionSubTypeMaster.CREDITCARD.name,
      transactionSubTypeMaster.CASA.name,
      transactionSubTypeMaster.DIRECT.name
    ).optional(),
    reference_no: Joi.string().optional(),
    status: Joi.string().allow('').valid(
      transactionStatusMaster.PENDING,
      transactionStatusMaster.HOLDED,
      transactionStatusMaster.APPROVED,
      transactionStatusMaster.SETTLED,
      transactionStatusMaster.FAILED,
      transactionStatusMaster.VOIDED,
      transactionStatusMaster.REFUNDED,
      transactionStatusMaster.CANCELLED
    ).optional(),
    start_date: Joi.date().optional(),
    end_date: Joi.date().optional(),
    offset: Joi.number().min(0).optional(),
    limit: Joi.number().min(1).optional()
  })
}

exports.findTransaction = {
  headers: Joi.object({
    'x-request-id': Joi.string().required(),
    'x-partner-id': Joi.string().allow('').optional(),
    'x-timezone': Joi.string().required(),
    'x-date-format': Joi.string().required()
  }).unknown(true)
}

exports.inquiryTransaction = {
  headers: Joi.object({
    'x-request-id': Joi.string().required(),
    'x-partner-id': Joi.number().required()
  }).unknown(true),
  params: Joi.object({
    id: Joi.number().required()
  })
}

exports.approveTransaction = {
  headers: Joi.object({
    'x-request-id': Joi.string().required(),
    'x-partner-id': Joi.string().required(),
    'x-timezone': Joi.string().required(),
    'x-date-format': Joi.string().required()
  }).unknown(true)
}

exports.voidTransaction = {
  headers: Joi.object({
    'x-request-id': Joi.string().required(),
    'x-partner-id': Joi.string().required()
  }).unknown(true),
  body: Joi.object({
    operator_id: Joi.number().required(),
    operator_name: Joi.string().required(),
    reason: Joi.string().required()
  })
}
