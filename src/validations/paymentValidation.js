const { Joi } = require('express-validation')

const userProxyMaster = require('../constants/masters/userProxyMaster')

exports.inquiryPayment = {
  headers: Joi.object({
    'x-request-id': Joi.string().required(),
    'x-partner-id': Joi.string().required(),
    'x-currency': Joi.string().required(),
    'x-timezone': Joi.string().required()
  }).unknown(true),
  body: Joi.object({
    order_id: Joi.string().required(),
    payer_proxy_type: Joi.string()
      .valid(
        userProxyMaster.WALLETID
      )
      .required(),
    payer_proxy_value: Joi.string()
      .when('payer_proxy_type', {
        is: userProxyMaster.WALLETID,
        then: Joi.string().length(15).required()
      }),
    payee_proxy_type: Joi.string()
      .valid(
        userProxyMaster.WALLETID
      )
      .required(),
    payee_proxy_value: Joi.string()
      .when('payee_proxy_type', {
        is: userProxyMaster.WALLETID,
        then: Joi.string().length(15).required()
      }),
    discount_code: Joi.string().optional(),
    fee: Joi.number().min(0).optional(),
    discount: Joi.number().optional(),
    amount: Joi.number().min(0).required(),
    expiry: Joi.number().min(1).max(30).optional()
  })
}

exports.confirmPayment = {
  headers: Joi.object({
    'x-request-id': Joi.string().required(),
    'x-partner-id': Joi.string().required(),
    'x-timezone': Joi.string().required()
  }).unknown(true),
  body: Joi.object({
    reference_no: Joi.string().required()
  })
}
