const { Joi } = require('express-validation')

exports.inquiryWithdrawCasa = {
  headers: Joi.object({
    'x-request-id': Joi.string().required(),
    'x-partner-id': Joi.string().required(),
    'x-currency': Joi.string().required(),
    'x-timezone': Joi.string().required()
  }).unknown(true),
  body: Joi.object({
    order_id: Joi.string().required(),
    wallet_id: Joi.string().required(),
    amount: Joi.number().min(1).required(),
    expiry: Joi.number().min(1).max(30).optional()
  })
}

exports.confirmWithdrawCasa = {
  headers: Joi.object({
    'x-request-id': Joi.string().required(),
    'x-partner-id': Joi.string().required(),
    'x-timezone': Joi.string().required()
  }).unknown(true),
  body: Joi.object({
    reference_no: Joi.string().required()
  })
}
