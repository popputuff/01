const { Joi } = require('express-validation')

const JoiDate = Joi.extend(require('@hapi/joi-date'))

const transactionSettlementBatchStatusMaster = require('../constants/masters/transactionSettlementBatchStatusMaster')

exports.findTransactionSettlementBatches = {
  headers: Joi.object({
    'x-request-id': Joi.string().required(),
    'x-partner-id': Joi.string().allow('').optional(),
    'x-timezone': Joi.string().required(),
    'x-date-format': Joi.string().required()
  }).unknown(true),
  query: Joi.object({
    start_date: JoiDate.date().format('YYYY-MM-DD').optional(),
    end_date: JoiDate.date().format('YYYY-MM-DD').optional(),
    batch_no: Joi.string().allow('').optional(),
    status: Joi.valid(
      transactionSettlementBatchStatusMaster.PENDING,
      transactionSettlementBatchStatusMaster.PROCESSING,
      transactionSettlementBatchStatusMaster.SETTLED
    ).allow('').optional(),
    limit: Joi.number().min(1).max(100).optional(),
    offset: Joi.number().min(0).optional()
  })
}

exports.findTransactionSettlementBatch = {
  headers: Joi.object({
    'x-request-id': Joi.string().required()
  }).unknown(true),
  params: Joi.object({
    id: Joi.number().required()
  })
}

exports.settleTransactionSettlementBatch = {
  headers: Joi.object({
    'x-request-id': Joi.string().required()
  }).unknown(true),
  params: Joi.object({
    id: Joi.number().required()
  }),
  body: Joi.object({
    operator_id: Joi.number().required(),
    operator_name: Joi.string().required()
  })
}
