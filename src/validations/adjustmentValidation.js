const { Joi } = require('express-validation')

const userProxyMaster = require('../constants/masters/userProxyMaster')

exports.inquiryAdjustment = {
  headers: Joi.object({
    'x-request-id': Joi.string().required(),
    'x-partner-id': Joi.string().required()
  }).unknown(true),
  body: Joi.object({
    order_id: Joi.string().required(),
    payer_proxy_type: Joi.string()
      .valid(
        userProxyMaster.WALLETID
      )
      .required(),
    payer_proxy_value: Joi.string()
      .when('payer_proxy_type', {
        is: userProxyMaster.WALLETID,
        then: Joi.string().length(15).required()
      }),
    payee_proxy_type: Joi.string()
      .valid(
        userProxyMaster.WALLETID
      )
      .required(),
    payee_proxy_value: Joi.string()
      .when('payee_proxy_type', {
        is: userProxyMaster.WALLETID,
        then: Joi.string().length(15).required()
      }),
    amount: Joi.number().min(1).required(),
    remark: Joi.string().required(),
    expiry: Joi.number().min(1).max(30).optional()
  })
}

exports.confirmAdjustment = {
  headers: Joi.object({
    'x-request-id': Joi.string().required(),
    'x-partner-id': Joi.string().required()
  }).unknown(true),
  body: Joi.object({
    reference_no: Joi.string().required()
  })
}
