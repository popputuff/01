const { Joi } = require('express-validation')

exports.topupPromptpay = {
  headers: Joi.object({
    'x-request-id': Joi.string().required(),
    'x-partner-id': Joi.string().required(),
    'x-currency': Joi.string().required(),
    'x-timezone': Joi.string().required()
  }).unknown(true),
  body: Joi.object({
    order_id: Joi.string().required(),
    wallet_id: Joi.string().length(15).required(),
    amount: Joi.number().min(1).required(),
    callback_url: Joi.string().required(),
    notify_url: Joi.string().required()
  })
}

exports.topupCreditCard = {
  headers: Joi.object({
    'x-request-id': Joi.string().required(),
    'x-partner-id': Joi.string().required(),
    'x-currency': Joi.string().required(),
    'x-timezone': Joi.string().required()
  }).unknown(true),
  body: Joi.object({
    order_id: Joi.string().required(),
    wallet_id: Joi.string().length(15).required(),
    amount: Joi.number().min(1).required(),
    callback_url: Joi.string().required(),
    notify_url: Joi.string().required()
  })
}

exports.topupDirect = {
  headers: Joi.object({
    'x-request-id': Joi.string().required(),
    'x-partner-id': Joi.string().required(),
    'x-currency': Joi.string().required(),
    'x-timezone': Joi.string().required()
  }).unknown(true),
  body: Joi.object({
    order_id: Joi.string().required(),
    wallet_id: Joi.string().length(15).required(),
    amount: Joi.number().min(1).required(),
    reference1: Joi.string().optional(),
    reference2: Joi.string().optional(),
    reference3: Joi.string().optional()
  })
}
