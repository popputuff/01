const { Joi } = require('express-validation')

const transactionStatusMaster = require('../constants/masters/transactionStatusMaster')

exports.notify = {
  headers: Joi.object({
    'x-request-id': Joi.string().required(),
    'x-partner-id': Joi.string().required(),
    'x-timezone': Joi.string().required(),
    'x-date-format': Joi.string().required()
  }).unknown(true),
  body: Joi.object({
    data: Joi.object({
      order_id: Joi.string().required(),
      approval_code: Joi.when('status', {
        is: transactionStatusMaster.APPROVED,
        then: Joi.string().length(6).required()
      }),
      failed_reason: Joi.when('status', {
        is: transactionStatusMaster.FAILED,
        then: Joi.string().required()
      }),
      status: Joi.valid(transactionStatusMaster.APPROVED, transactionStatusMaster.FAILED)
    }).unknown(true),
    sign: Joi.string().required()
  })
}
