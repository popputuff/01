const { Joi } = require('express-validation')

const transactionStatusMaster = require('../constants/masters/transactionStatusMaster')
const transactionSubTypeMaster = require('../constants/masters/transactionSubTypeMaster')
const transactionTypeMaster = require('../constants/masters/transactionTypeMaster')

exports.history = {
  headers: Joi.object({
    'x-request-id': Joi.string().required(),
    'x-partner-id': Joi.string().required(),
    'x-timezone': Joi.string().required(),
    'x-date-format': Joi.string().required(),
    'x-email': Joi.string().optional()
  }).unknown(true),
  query: Joi.object({
    type: Joi.string().allow('').valid(
      transactionTypeMaster.TOPUP.name,
      transactionTypeMaster.TRANSFER.name,
      transactionTypeMaster.PAYMENT.name,
      transactionTypeMaster.WITHDRAW.name,
      transactionTypeMaster.ADJUSTMENT.name
    ).optional(),
    sub_type: Joi.string().allow('').valid(
      transactionSubTypeMaster.CLOSELOOP.name,
      transactionSubTypeMaster.PROMPTPAY.name,
      transactionSubTypeMaster.CREDITCARD.name,
      transactionSubTypeMaster.CASA.name
    ).optional(),
    order_id: Joi.string().optional(),
    reference_no: Joi.string().optional(),
    user_uid: Joi.string().optional(),
    wallet_id: Joi.string().optional(),
    status: Joi.string().allow('').valid(
      transactionStatusMaster.PENDING,
      transactionStatusMaster.HOLDED,
      transactionStatusMaster.APPROVED,
      transactionStatusMaster.SETTLED,
      transactionStatusMaster.FAILED,
      transactionStatusMaster.VOIDED,
      transactionStatusMaster.REFUNDED,
      transactionStatusMaster.CANCELLED
    ).optional(),
    date: Joi.date().optional(),
    start_date: Joi.date().optional(),
    end_date: Joi.date().optional(),
    offset: Joi.number().min(0).optional(),
    limit: Joi.number().min(1).optional()
  })
}
