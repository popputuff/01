require('dotenv').config()

const express = require('express')
const bodyParser = require('body-parser')

const httpError = require('./constants/errors/httpError')

const logController = require('./controllers/logController')

const { HttpError } = require('./helpers/error')
const winston = require('./helpers/winston')

const routers = require('./routers')

const ENVIRONMENT = process.env.ENVIRONMENT
const HOSTNAME = process.env.HOSTNAME
const PORT = Number(process.env.PORT)

const app = express()

app.enable('trust proxy')
app.disable('x-powered-by')

app.use(bodyParser.json({ limit: '5mb' }))
app.use((req, res, next) => {
  res.header('Cache-Control', 'no-cache, no-store, max-age=0, must-revalidate')
  res.header('Strict-Transport-Security', 'max-age=15778463; includeSubDomains; preload')
  res.header('X-Content-Type-Options', 'nosniff')
  res.header('X-Frame-Options', 'DENY')
  res.header('X-XSS-Protection', '1; mode=block')
  next()
})
app.use(logController.createRequestLog())
app.use(routers)
app.use((req, res, next) => {
  if (!res.headersSent) {
    return next(new HttpError(httpError.ERR_HTTP_404))
  }
  return next()
})
app.use(logController.createResponseLog())
app.use(logController.createErrorLog())

app.listen(PORT, HOSTNAME, () => {
  winston.info(`${ENVIRONMENT.charAt(0).toUpperCase() + ENVIRONMENT.slice(1)} Environment`)
  winston.info(`Service Started on ${PORT}`)
})
